# API POLICIES

1. if there is no other strict rules/issues about which API have to be, its has to be REST API
2. use plural form for entities; `/items/12345` is good (`/item/12345` is bad)
3. use mostly POST, GET
4. DELETE (or PUT, OPTIONS, HEAD) may be used, but POST duplication required: `DELETE /items/12345` must be duplicated by `POST /items/12345/delete`
5. all POST bodies must be JSON
6. root object must be JSON object, not JSON array: good is `{"list": [...]}` (bad: `[]`), 
however `{"id": 12345, "someItems": []}` is ok, `"list"` is not mandatory inside other object but **is** mandatory for root-lists

See as well: https://medium.com/better-programming/restful-api-design-step-by-step-guide-2f2c9f9fcdbf
