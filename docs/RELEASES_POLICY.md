# RELEASES POLICY #

Digital Harbour project uses classic `git flow` practice.

See [git flow cheat-sheet](https://danielkummer.github.io/git-flow-cheatsheet/) for quick help.

## Common release types

### Major release: 

    When:
    * vast functionality change from previous release
    * client-api or admin-console-api changed and not supported to previous

    Example-1: 
    * we are on 0.9.0 release and think all functions are ready for launch
    * we are creating 1.0.0 and starting to test
    
    Example-2:
    * we are on 1.2.3 release
    * we worked so hard on `features/big-boom-secret-features-2.0` and seems to finished it    
    * we are merging this feature to develop and going to `2.0.0` (from now all `1.2.3` fixes are **hot**fixes)     

### 2. Minor release:
    When:    
    * some sprint finished with some new functionality implemented
    * some feature added but all API compatibility conditions are met
    
    Example: 
    * we are on release `0.2.0`
    * we finished some sprint and planning to deliver some new features
    * we are creating `0.3.0` release and going to test it

### 3. Bugfix release

    When:
    * we are on 1.2.3 and some bug or very minor features fixed (not urgent) and no big features was merged to develop now   
    * running `git flow releaase start 1.2.4` -> release notes, testing -> `git flow release finish 1.2.4`
    Bugfix release like feature release: started from **develop** branch
    
### 4. Hotfixes release

    When-1:
    * we are on 1.2.3 but urgent bug found and must be fixed right now on production
    * starting `git flow hotfix start 1.2.3-hotfix1` -> fixing, testing -> `git flow hotfix finish 1.2.3-hotfix1`
    
    When-2:
    * we are on `1.2.3-hotfix1` just merged some big feature in `develop` and testing goes slowly and not very successful
    * at the same time there are some important issues from customer, not urgent, but they cannot wait till we finish big new feature testing
    * running `git flow hotfix start 1.2.3-hotfix2` and so on