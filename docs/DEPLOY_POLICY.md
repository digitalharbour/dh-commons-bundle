# DEPLOY POLICY


### Must use [Git info plugin](https://github.com/n0mer/gradle-git-properties) for [spring actuator](https://www.baeldung.com/spring-boot-actuators)
**The `build.gradle` of root project must be on same level where .git folder is.**
Use in your spring application attaching plugin:
```
plugins {
    id "com.gorylenko.gradle-git-properties" version "2.0.0"
}
...

dependencies {
    // https://mvnrepository.com/artifact/org.springframework.boot/spring-boot-starter-actuator
    compile group: 'org.springframework.boot', name: 'spring-boot-starter-actuator', version: versions.springBoot
}
```
and if you want full details about git commit use in your `application.properties`
```properties
# DevOps
management.info.git.mode=full
```

### Should use in your `application.properties` the spring ability for using env variables

Like:
```properties
# set MYAPP_PORT env variable or by default 8082
server.port=${MYAPP_PORT:8082}
```

It is very convenient way for both DevOps and other developer for running app the way they want 
using unified and simple mechanism of OS environment variables (devops) or IDE eun configurations 
(usually allows set env variables easily).

Use it at least for most popular deployment configuration params:
* server port
* database credentials
* file storage (if used)
* other external services (smtp, smpp)
* external API endpoints (sibling microservices, some customer's APIs, etc.)

### Must use `org.owasp.dependencycheck` plugin

```groovy
plugins {
    id "com.gorylenko.gradle-git-properties" version "2.0.0"
}
```

After injecting plugin call `gradlew dependencyCheckAnalyze` which must give in result 
(it takes some time) like:
```
Found 0 vulnerabilities in project commons-web
``` 

If not, check the `build/reports/dependency-check-report.html` and consider to update or 
get rid libraries which cause vulnerabilities.

 
