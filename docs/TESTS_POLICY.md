# TESTS POLICY #

## SHOULD

* ...write test first if you have even a little doubt about what is the name/signature of new method, exception, etc.
* ...write test for **every** found bug (even if you found it yourself!)
* ...separate integration tests from unit-test (see below the difference explanation)

## SHOULD NOT

* ...cover every method in the name of "full coverage"
* ...write essential business service without functional test
* ...write slow integration tests into src/java/test

## INTEGRATION vs UNIT 

Good explanation for DH-typical case is [here](https://stackoverflow.com/questions/32223490/are-springs-mockmvc-used-for-unit-testing-or-integration-testing):
```
All forms (of mock mvc tests) are actually integration tests since you are testing 
the integration of your code with the Spring DispatcherServlet and supporting infrastructure.
```

Spring [about integration testing](https://docs.spring.io/spring/docs/current/spring-framework-reference/testing.html#integration-testing):
```
It is important to be able to perform some integration testing without requiring deployment to your 
application server or connecting to other enterprise infrastructure. 
This will enable you to test things such as:
* The correct wiring of your Spring IoC container contexts.
* Data access using JDBC or an ORM tool. This would include such things as the correctness of SQL statements, 
Hibernate queries, JPA entity mappings, etc.

The Spring Framework provides first-class support for integration testing in the spring-test module.
...

This testing does not rely on an application server or other deployment environment. 
Such tests are slower to run than unit tests but much faster than the equivalent Selenium 
tests or remote tests that rely on deployment to an application server
```

[Good explanation](https://www.petrikainulainen.net/programming/gradle/getting-started-with-gradle-integration-testing/) 
why it is important to separate integration tests from unit tests and step-by-step manual for gradle project. 




TODO: provide details about mocking db, services, etc.