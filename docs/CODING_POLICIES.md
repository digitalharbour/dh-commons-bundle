# CODING POLICY / RULES / PRINCIPLES #

1. We are **strongly** null-intolerant team. Every `return null` is bad bad sin. 
Null is evil. Use `throw IllegalArgumentException` and `throw UnsupportedStateException` instead.
Use constructors to force user of your class to instantiate fulfilled class (see p5. in details).
Use Optional as getter to force user of your class think twice about it really is optional and could be not set. 

2. Think **twice** before do something as aspect. 
If inheritance gives the same "one-line code like `super()`" it is **always** better than `@DoForAllOperationsLikeThis`

3. JavaDoc comments are **mandatory** in public classes, methods and especially interfaces.
If you think there is not important for documenting it, think about remove `public` from it

4. Using **package scope** visibility restriction is good.
Do not place all "similar" classes in same package cause they are just similar. 
Do place functionally coupled classes in one package and let they call each other in package scope. 
So, hide "coupleness" inside the package.

5. Our business logic code is **framework / container agnostic**.
That means anybody can easily run any logic from simple main() method.
E.g. that means no @Autowire is allowed for spring, components must be initialized from constructor 
(not from setter injections, see: [why it's better](https://stackoverflow.com/questions/21218868/explain-why-constructor-inject-is-better-than-other-options))
Better way for being container-agnostic is write functional and unit tests, which do not use spring.

5.1. **Constructor-based DI is better** then setter-based
[Spring says](https://docs.spring.io/spring/docs/current/spring-framework-reference/core.html#beans-setter-injection): 
```
Since you can mix constructor-based and setter-based DI, it is a good rule of thumb to use constructors 
for mandatory dependencies and setter methods or configuration methods for optional dependencies. 
...
The Spring team generally advocates constructor injection as it enables one to implement application 
components as immutable objects and to ensure that required dependencies are not null...
...
Setter injection should primarily only be used for optional dependencies that can be assigned 
reasonable default values within the class.
...
```
That is again about the same in p5. Do use constructors heavily to instantiate consistent robust class. Only optional 
properties could be absent in constructors and may be instantiated.   


... TO BE CONTINUE ...
