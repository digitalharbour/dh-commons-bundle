# LOGGING POLICY #

Logging policy for system components.

## Operational log

Operational log is essential part of system **functionality**, not just "some logs to developers need".
Below described WHAT must be logged and HOW it must be logged.

### WHAT must be logged in Operational Log

Here is the list of events which MUST be logged.

1. Every **call from outside** must be logged as `INFO`
    * any call from client API is outside call
    * any call from admin API is outside call
2. Every **call outside** must be logged as `INFO`
    * any integration API call is outside call    
3. Every **error from outside**
    * any integration API call returned error must be logged as `WARN` or `ERROR`
4. Every **error sent outside**
    * any error sent to client API must be logged as `ERROR` or `WARN` 
5. Nothing else must not be logged as `INFO` or `WARN`, just:
    * `ERROR` when something unexpected happened
    * `FATAL` when something essential not works at all
    * `DEBUG` for free :)
    
### WARN or ERROR?

**WARN if** something not works but it is ok for some cases, users are able to use system, 
the situation not required immediately attention from support, however could be if it happens too often e.g.:

* requesting expired session
* accepting login with wrong login or password
* requested contract ID not exists
* some params-validation error occurred in integration layer (requested product type not exists)


**ERROR if** something definitely goes wrong and users are really affected, they unable do some
 functionality, incident must be registered for support team e.g.:

* integration layer unable to perform request and it is not validation error but i/o error
* our API returns HTTP 500
* email not sent due SMTP service returned error

**FATAL if** same as ERROR but this error means that *most of users* unable to perform *essential* functionality 
and it demands *immediately* support team invasion, e.g.:

* unable to connect to database
* unable to read essential config
* unable to call essential integration layer server (auth-server)
* email not sent due SMTP service returned error IF you are email-sender application 
and it is your essential functionality

### HOW Operational Log records looks

Every log-event must contain 

* timestamp
* actor who does this action
* essential context (like IP address of external client)

It's recommended to have some MySession or MyContext objects and make some LogHelper class 
which works with this context. This context should keep current actor, essential context data, etc.
LogHelper have to print all this context in standard way.
