
# WHY USE THIS?

*Why do not use spring-.... for all of this?*

Because spring is mostly **web-app-framework** and **DI-framework**. 
Web-app framework is for delivery **existing** DOMAIN logic to web-app, right?
DI-framework is for free instantiation and injections into **existing** DOMAIN logic, right?

*But all these commons are about existing your business logic ... to use it with web-framework, DI-framework, anything-framework.*

All these commons supposed to be the **ground** for building DOMAIN classes. 
Not good to use web-specific classes from Spring (or other web-app-framework) into your own domain logic. 
Because domain logic must work without spring as well. It must work right from `main()` and even without spring or other DI-container 
(in that case it could be difficult to instantiate all beans/services without DI but it must work).
It's not about "do not use spring", it's about "use spring but for its natural goals": for delivering domain logic to web and
for providing DI-coupling your business classes (btw spring nothing demands from your domain classes! and it's great and right: 
your domain classes must be clean from spring... spring-annotations is maximum allowed there).

See cleancoder blog article about [clean architecture](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html):
---
1. Independent of Frameworks. The architecture **does not depend on the existence of some library** of feature laden software. 
This allows you to **use such frameworks as tools, rather than having to cram your system into their limited constraints**.
2. Testable. The business rules **can be tested without the UI, Database, Web Server**, or any other external element.
3. Independent of UI. The UI can change easily, without changing the rest of the system. 
**A Web UI could be replaced with a console UI, for example**, without changing the business rules.
4. Independent of Database. You can swap out Oracle or SQL Server, for Mongo, BigTable, CouchDB, or something else. **Your business rules are not bound to the database**.
5. Independent of any external agency. In fact your business rules simply don’t know anything at all about the outside world.
---

## Exception policy 

There are good hierarchy of business-layer `CommonUserException` which provide localization-ready 
error messages for using at business (service) layer.
These exceptions has contract: if something throw it, the message have to be delivered to end-user (API-user).
So, this is strict list of exceptions with hierarchy for the most typical business cases:

* [`ValidationUserException`](commons/src/main/java/com/teamharbour/commons/exceptions/ValidationUserException.java): allows build good-structured multi-param response, use it for collecting list of
 form validation bad-params; as well it wraps javax.validation results (map it to HTTP 400)
* [`BadParamUserException`](commons/src/main/java/com/teamharbour/commons/exceptions/BadParamUserException.java): if just one parameter is wrong, used as item inside `ValidationUserException` 
(map it to HTTP 400)
* [`IllegalOperationUserException`](commons/src/main/java/com/teamharbour/commons/exceptions/IllegalOperationUserException.java): if there is not specific parameters but whole operation is bad/wrong/illegal
(map it to HTTP 400)
* [`AuthenticationUserException`](commons/src/main/java/com/teamharbour/commons/exceptions/AuthenticationUserException.java): should be thrown when authentication error happened (map it to HTTP 401)
* [`NoAccessUserException`](commons/src/main/java/com/teamharbour/commons/exceptions/NoAccessUserException.java): should be thrown when user is authenticated but not allowed to do something (map it to HTTP 403)
* [`NotFoundUserException`](commons/src/main/java/com/teamharbour/commons/exceptions/NotFoundUserException.java): should be thrown if something supposed to be found not found 
(please, do not use `return null;`. Never. return Optional or throw `NotFoundUserExcpeiton`)

## Essential entities

* [`ConfigurationLoader`](commons/src/main/java/com/teamharbour/commons/BinaryData.java): very useful for config externalization; 
it allows to load different config files (it's not not always good idea to store all-all properties into application.properties) 
from some "home" directory (e.g. "MYAPP_HOME=/home/me/.myapp"); ConfigLoader try to find requested config there in MYAPP_HOME and 
if not found, automatically copy resource with same name from jar into this directory. So, kinda auto-configuring: after first start all configs are copied into HOME dir
* [`BinaryData`](commons/src/main/java/com/teamharbour/commons/BinaryData.java): container for binary data; just reminder that binary data is not just data but some metadata: content type, optionally default filename, etc.
* [`AbstractDbEntity`](commons/src/main/java/com/teamharbour/commons/AbstractDbEntity.java): have to be parent of all DB-entities, have createdAt, updatedAt (both with auto-update), UUID as ID
and right equal behavior (check by id, not by all fields, cause if order1.id = order2.id it *is* same order even if in order1 some service deserialize more date than in order2)
* [`AnyAttributesObject`](commons/src/main/java/com/teamharbour/commons/AnyAttributesObject.java): extend your value-object from this to obtain "any unknown attribute in json -> to HashMap" behavior: 
in general, all "SomethingForm" objects should use it to collect unpredictable attributes coming from outside REST call to hashmap 
(e.g. you do not know right now which attributes client will send you but do not want to just ignore them)
* [`CommonForm`](commons/src/main/java/com/teamharbour/commons/CommonForm.java): just empty child of `AnyAttributesObject` to use as root class for all "some form"
* [`JsonListWrapper`](commons/src/main/java/com/teamharbour/commons/JsonListWrapper.java): must use it before return kind of array outside; arrays must be contined into `{"list": []}` to be able to add some properties like metadata 
or something aggregated about this array without breaking json structure
* [`I18nString`](commons/src/main/java/com/teamharbour/commons/JsonListWrapper.java): should use if (despite the [Internationalization policy](docs/I18N.md)) 
you have to send multilanguage data right from server (e.g. some backend endpoint already has multilanguage data and you cannot predict it's messageKeys); this simple class
allows to build multi-language string with adding any languages with useful builder. It's json-friendly, e.g.

```json
{
  "values" : {
    "uk" : "Ім'я",
    "en" : "Name",
    "es" : "Nombre"
  },
  "defaultLang" : "es",
  "defaultValue" : "Nombre"
}
```

* [`CommonsUtils`](): many useful utils like:
    * `maskString` for easy transforming "Password" to "P...d" or "Some very long string" to "Som...ing" or just "Some..."
    * `loadGitInfo` for display current version and commit id (if you use right the [Deploy policy](docs/DEPLOY_POLICY.md))
    * many json-serialization, parse tools like `parseAsDateOrDateTime`  
    * 

TODO: to be continue...

# POLICIES

* [API policies](docs/API_POLICY.md)
* [Writing tests](docs/TESTS_POLICY.md)
* [Coding policies](docs/CODING_POLICIES.md)
* [Logging policy](docs/LOGGING_OPERATIONAL.md)
* [Internationalization](docs/I18N.md)
* [Deploy policy](docs/DEPLOY_POLICY.md)
* Exception policy (see above)


# MODULES

* [commons](commons/README.md): common lib with useful utils to build your business-logic
* [commons-web](commons-web/README.md): relatively small toolset for helping to use commons-based business logic with spring-mvc/spring-boot development

# RELEASES

## RELEASE 0.8.0

**Changelog**

* First stable release used in several TemaHarbour projects

**Install**

* See [INSTALL](#INSTALL) section

## RELEASE 0.9.0 (IN PROGRESS)

**Changelog**

* Remove deprecated classes and warnings
* Add page-list for allow pagination info
* ...

**Install**

* See [INSTALL](#INSTALL) section


# INSTALL

* Clone this repository

```
git clone 
```
 
 * Build projects 
 ```
 ./gradlew clean build
 ```
 * Install them to your local repository with sources and javadoc
 
```
 ./gradlew publishToMavenLocal
```
 
 * Use it in your project:
 
 Gradle:
 
```
repositories {
    mavenLocal()
    ...        
}

...
    
dependencies {
    compile group: 'com.teamharbour.digitalharbour', name: 'dh-commons', version: '0.8.0'
    compile group: 'com.teamharbour.digitalharbour', name: 'dh-commons-web', version: '0.8.0'
}
```
