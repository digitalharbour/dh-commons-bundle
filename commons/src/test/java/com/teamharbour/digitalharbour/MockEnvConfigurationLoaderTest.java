package com.teamharbour.digitalharbour;

import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class MockEnvConfigurationLoaderTest {

    @Test
    void test_unique_home_dirs() throws IOException {
        MockEnvConfigurationLoader x = new MockEnvConfigurationLoader(this.getClass());
        MockEnvConfigurationLoader y = new MockEnvConfigurationLoader(this.getClass());

        assertNotEquals(x.getHomeDir(), y.getHomeDir());

        x.purgeHomeDir();
        y.purgeHomeDir();
    }

}