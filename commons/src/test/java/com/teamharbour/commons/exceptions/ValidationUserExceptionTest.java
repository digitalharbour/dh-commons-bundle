package com.teamharbour.commons.exceptions;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.ValidatorFactory;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Collections;
import java.util.Set;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;

class ValidationUserExceptionTest {

    private static class SomeObject {
        @NotNull(message = "must_not_be_empty")
        @Size(min = 3, max = 10)
        private String x;
        @NotNull
        @Max(100)
        @Min(1)
        private Integer y;

        SomeObject(@NotNull @Size(min = 3, max = 10) String x, @NotNull @Max(100) @Min(1) Integer y) {
            this.x = x;
            this.y = y;
        }
    }

    ValidatorFactory validatorFactory;
    ObjectMapper mapper;

    @BeforeEach
    void setUp() throws Exception {
        validatorFactory = Validation.buildDefaultValidatorFactory();
        mapper = new ObjectMapper().findAndRegisterModules();
    }

    @Test
    void addConstraintViolations() throws JsonProcessingException {

        SomeObject badObj1 = new SomeObject(null, null);
        Set<ConstraintViolation<Object>> violations = validatorFactory.getValidator().validate(badObj1);

        assertThat("Errors found", violations.size(), greaterThan(1));


        ValidationUserException e = new ValidationUserException("Some error").addConstraintViolations(violations);
        assertThat("Bad params qty same as violations qty", e.getBadParams().size(), equalTo(violations.size()));

        e.addConstraintViolations(Collections.singleton(new ValidationUserException("Some other error")));

        System.out.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(e));

    }

}