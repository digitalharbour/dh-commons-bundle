package com.teamharbour.commons.exceptions;

import org.junit.jupiter.api.Test;

import static com.teamharbour.commons.CommonsUtils.arrayToMap;
import static org.junit.jupiter.api.Assertions.*;

class CommonUserExceptionTest {

    String msgKey = "error_of_test";
    String msgPattern = "Error in this {} and that {}";

    @Test
    void test_parameters_building() {

        // constructor with map values
        CommonUserException x = new InternalErrorUserException(msgKey, msgPattern);
        assertEquals(x.getMessageKey(), msgKey);
        assertEquals(x.getDefaultMessage(), msgPattern);
        assertTrue(x.getParametersMap().isEmpty());
        assertEquals(0, x.getParametersList().length);

        // constructor with list of (name, value)
        x = new InternalErrorUserException(msgKey, msgPattern, arrayToMap("p9", "v9", "p1", "v1"));
        assertEquals(x.getMessageKey(), msgKey);
        assertEquals(x.getDefaultMessage(), "Error in this v9 and that v1");
        assertEquals(2, x.getParametersMap().size());
        assertEquals(2, x.getParametersList().length);

        // constructor with map values
        x = new InternalErrorUserException(msgKey, msgPattern, arrayToMap("p9", "v9", "p1", "v1"));
        assertEquals(x.getMessageKey(), msgKey);
        assertEquals(x.getDefaultMessage(), "Error in this v9 and that v1");
        assertEquals(2, x.getParametersMap().size());
        assertEquals(2, x.getParametersList().length);


    }

}