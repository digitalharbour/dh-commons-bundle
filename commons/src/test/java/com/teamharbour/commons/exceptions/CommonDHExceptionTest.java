package com.teamharbour.commons.exceptions;

import com.teamharbour.commons.CommonsUtils;
import org.junit.jupiter.api.Test;

import java.util.Map;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;


public class CommonDHExceptionTest {

    @Test
    public void getParametersMap() {
        CommonUserException e = new ValidationUserException(
                "wrong_way",
                "You choose wrong way",
                CommonsUtils.arrayToMap("par1", "val1", "par2", "val2")
        );

        assertThat(e.getParametersMap(), notNullValue());
        assertThat(e.getParametersMap().get("par1"), equalTo("val1"));
        assertThat(e.getParametersMap().get("par2"), equalTo("val2"));
        assertThat(e.getParametersMap().get("par99"), nullValue());

    }

    @Test
    public void getParametersList() {
        CommonUserException e = new ValidationUserException(
                "wrong_way",
                "You choose wrong way",
                CommonsUtils.arrayToMap("par3", "val3", "par1", "val1", "par2", "val2")
        );

        String[] listArr = e.getParametersList();

        assertThat("sorting check 3", listArr[0], equalTo("val3"));
        assertThat("sorting check 1", listArr[1], equalTo("val1"));
        assertThat("sorting check 2", listArr[2], equalTo("val2"));
    }
}