package com.teamharbour.commons;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Currency;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;

public class JsonListWrapperTest {

    private final Currency USD = Currency.getInstance("USD");
    private final Currency EUR = Currency.getInstance("EUR");
    private ObjectMapper mapper;

    @BeforeEach
    public void setUp()  {
        mapper = new ObjectMapper().findAndRegisterModules();
    }

    @Test
    public void testJsonSerializationCommonCase() throws IOException {

        JsonListWrapper<Currency> x = new JsonListWrapper<>(USD, EUR);
        String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(x);
        System.out.println(json); // visual control

        DocumentContext jsonDoc = JsonPath.parse(json);
        assertThat("Contains usd", jsonDoc.read("$['list'][0]").toString(), equalTo("USD"));
        assertThat("Contains usd", jsonDoc.read("$['list'][1]").toString(), equalTo("EUR"));

    }

    @Test
    public void testAlternativeEntryName() throws JsonProcessingException {

        JsonListWrapper<Currency> x = new JsonListWrapper<>(USD, EUR).listEntryName("currencies");
        String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(x);
        System.out.println(json); // visual control

        DocumentContext jsonDoc = JsonPath.parse(json);
        assertThat("Contains /currencies/usd", jsonDoc.read("$['currencies'][0]").toString(), equalTo("USD"));
        assertThat("Contains /currencies/eur", jsonDoc.read("$['currencies'][1]").toString(), equalTo("EUR"));

    }

    @Test
    public void testEmptyList() throws JsonProcessingException {
        JsonListWrapper<Currency> x = new JsonListWrapper<Currency>().listEntryName("currencies");
        String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(x);
        System.out.println(json); // visual control

        DocumentContext jsonDoc = JsonPath.parse(json);
        assertThat("Contains /currencies", jsonDoc.read("$['currencies']"), notNullValue());

    }

    @Test
    public void testListWithCount() throws JsonProcessingException {

        JsonListWrapper<Currency> x = new JsonListWrapper<>(USD, EUR).listEntryName("currencies").withCount();
        String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(x);
        System.out.println(json); // visual control

        DocumentContext jsonDoc = JsonPath.parse(json);
        assertThat("Contains /count", jsonDoc.read("$.count").toString(), equalTo("2"));
        assertThat("Contains /currencies/usd", jsonDoc.read("$.currencies[0]").toString(), equalTo("USD"));
        assertThat("Contains /currencies/eur", jsonDoc.read("$.currencies[1]").toString(), equalTo("EUR"));
    }
}