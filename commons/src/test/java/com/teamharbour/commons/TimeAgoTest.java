package com.teamharbour.commons;

import org.junit.jupiter.api.Test;

import java.util.Calendar;
import java.util.Date;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;


class TimeAgoTest {

    @Test
    void toRelative() {
        assertThat(TimeAgo.toRelative(0, 0), is("0 seconds ago"));
        assertThat(TimeAgo.toRelative(0, 6), is("0 seconds ago"));
        assertThat(TimeAgo.toRelative(-1, 6), is("0 seconds ago"));
        assertThat(TimeAgo.toRelative(1000, 6), is("1 second ago"));
        assertThat(TimeAgo.toRelative(601000, 1), is("10 minutes ago"));
        assertThat(TimeAgo.toRelative(601000, 2), is("10 minutes, 1 second ago"));
        assertThat(TimeAgo.toRelative(600000 * 6 + 1000, 2), is("1 hour, 1 second ago"));
        assertThat(TimeAgo.toRelative(600000 * 6 + 61000, 3), is("1 hour, 1 minute, 1 second ago"));
        assertThat(TimeAgo.toRelative(Long.MAX_VALUE, 1000), is("292471208 years, 8 months, 1 week, 7 hours, 12 minutes, 55 seconds ago"));
        assertThat(TimeAgo.toRelative(Long.MAX_VALUE, 0), is("292471208 years, 8 months, 1 week, 7 hours, 12 minutes, 55 seconds ago"));
        assertThat(TimeAgo.toRelative(Long.MAX_VALUE, 1), is("292471208 years ago"));
        assertThat(TimeAgo.toRelative(Long.MAX_VALUE, 2), is("292471208 years, 8 months ago"));
        assertThat(TimeAgo.toRelative(Long.MAX_VALUE, 3), is("292471208 years, 8 months, 1 week ago"));
        assertThat(TimeAgo.toRelative(Long.MAX_VALUE, 4), is("292471208 years, 8 months, 1 week, 7 hours ago"));
        assertThat(TimeAgo.toRelative(Long.MAX_VALUE, 5), is("292471208 years, 8 months, 1 week, 7 hours, 12 minutes ago"));
        assertThat(TimeAgo.toRelative(Long.MAX_VALUE, 6), is("292471208 years, 8 months, 1 week, 7 hours, 12 minutes, 55 seconds ago"));
    }

    @Test
    void toRelativeNoMaxLevel() {
        assertThat(TimeAgo.toRelative(0), is("0 seconds ago"));
        assertThat(TimeAgo.toRelative(7777), is("7 seconds ago"));
        assertThat(TimeAgo.toRelative(7777777), is("2 hours, 9 minutes, 37 seconds ago"));
        assertThat(TimeAgo.toRelative(-1), is("0 seconds ago"));
        assertThat(TimeAgo.toRelative(Long.MAX_VALUE), is("292471208 years, 8 months, 1 week, 7 hours, 12 minutes, 55 seconds ago"));
    }

    @Test
    void toRelativeStartDateEndDateException() {
        assertThrows(IllegalArgumentException.class,
                () -> {
                    Date date1 = new Date();
                    Date date2 = new Date(date1.getTime()-1);
                    assertThat(TimeAgo.toRelative(date1, date2, 6), is("0 seconds ago"));
                },
                "Start date must be greater than end");
    }

    @Test
    void toRelativeStartDateEndDate() {
        // 1 sec
        Date date1 = new Date();
        Date date2 = new Date();
        Calendar instance = Calendar.getInstance();
        instance.setTimeInMillis(date2.getTime());
        instance.add(Calendar.SECOND, 1);
        date2 = instance.getTime();
        assertThat(TimeAgo.toRelative(date1, date2, 6), is("1 second ago"));
    }

    @Test
    void toRelativeStartDateEndDateNoMaxLevel() {
        // 1 sec
        Date date1 = new Date();
        Date date2 = new Date();
        Calendar instance = Calendar.getInstance();
        instance.setTimeInMillis(date2.getTime());
        instance.add(Calendar.MINUTE, 1);
        instance.add(Calendar.SECOND, 1);
        date2 = instance.getTime();
        assertThat(TimeAgo.toRelative(date1, date2), is("1 minute, 1 second ago"));
    }
}