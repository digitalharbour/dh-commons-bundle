package com.teamharbour.commons;

import com.teamharbour.digitalharbour.MockEnvConfigurationLoader;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;


class ConfigurationLoaderTest {

    private MockEnvConfigurationLoader loader;

    @BeforeEach
    void setUp() {
        loader = new MockEnvConfigurationLoader(this.getClass());
    }

    @Test
    void loadConfig() throws IOException {

/* some debug
        for (String k:System.getenv().keySet()) {
            System.out.println(k + "=" + System.getenv(k));
        }

        ClassLoader classLoader = this.getClass().getClassLoader();
        File file = new File(classLoader.getResource("lorem_ipsum.txt").getFile());
        assertTrue(file.exists());

        System.out.println("1:"+this.getClass().getClassLoader().getResource("test-config.properties"));


        //System.out.println("2:"+this.getClass().getResource("com/teamharbour/digitalharbour/test-config.properties"));
        System.out.println("2:"+this.getClass().getResource("test-config.properties"));
*/

        InputStream input = loader.loadConfig("test-config.properties");
        assertThat(input, CoreMatchers.notNullValue());

        String text = loader.loadConfigAsText("test-config.properties");
        assertThat(text.trim(), CoreMatchers.equalTo("test1=value1"));

    }

    @Test
    void test_deep_resource() throws IOException {
        String x = loader.loadConfigAsText("dir/to/resource.txt");
        assertThat(x, equalTo("Deep resource"));
    }

    @AfterEach
    void tearDown() throws Exception {
        // cleaning config and directory
        loader.purgeHomeDir();
    }
}