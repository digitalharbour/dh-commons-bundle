package com.teamharbour.commons;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class PagingListTest {

    private static final List<String> LIST_OF_THREE = Arrays.asList("A", "B", "C") ;

    @Test
    void json_serialization() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper().findAndRegisterModules();

        PagingOptions pg = PagingOptions.createOffsetAndLimit(10,5);
        PagingList<String> tst = new PagingList<>(LIST_OF_THREE, pg, 100);

        System.out.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(tst));

        pg = PagingOptions.createPageAndLimit(2,3);
        tst = new PagingList<>(LIST_OF_THREE, pg, 100);

        System.out.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(tst));

        assertTrue(true); // we are here and that is success of serialization

    }

    @Test
    void obtain_same_list_as_given() {

        PagingOptions pg = PagingOptions.createOffsetAndLimit(10,20);
        PagingList<String> pagingList = new PagingList<>(LIST_OF_THREE, pg, 100);

        assertEquals(3, pagingList.getList().size());
    }

    @Test
    void error_if_list_bigger_than_paging_options_limit() {

        PagingOptions pg = PagingOptions.createOffsetAndLimit(10,2);

        assertThrows(IllegalArgumentException.class,
                ()-> new PagingList<>(LIST_OF_THREE, pg, 100),
                "Item list longer than options!");
    }


    @Test
    void has_next_if_total_big() {

        PagingOptions pg = PagingOptions.createOffsetAndLimit(10,3);
        PagingList<String> pagingList = new PagingList<>(LIST_OF_THREE, pg, 14);

        assertTrue(pagingList.isHasNext(), "Must has next cause 10 and 3 less than 14");
    }

    @Test
    void has_no_next_if_total_equal() {

        PagingOptions pg = PagingOptions.createOffsetAndLimit(10,3);
        PagingList<String> pagingList = new PagingList<>(LIST_OF_THREE, pg, 13);

        assertFalse(pagingList.isHasNext(), "Must not has next cause 10 and 3 is over = 13");
    }

    @Test
    void must_have_prev_if_offset_not_zero() {
        PagingOptions pg = PagingOptions.createOffsetAndLimit(1,3);
        PagingList<String> pagingList = new PagingList<>(LIST_OF_THREE, pg, 4);

        assertTrue(pagingList.isHasPrevious(), "Must have prev cause offset > 0");
    }

    @Test
    void must_not_have_prev_if_offset_is_zero() {
        PagingOptions pg = PagingOptions.createOffsetAndLimit(0,3);
        PagingList<String> pagingList = new PagingList<>(LIST_OF_THREE, pg, 4);

        assertFalse(pagingList.isHasPrevious(), "Must not have prev cause offset = 0");
    }

}