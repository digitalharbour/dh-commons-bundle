package com.teamharbour.commons;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class CommonCategoryTest {

    @Test
    public void testToAndFromJson() throws IOException {
        ObjectMapper mapper = new ObjectMapper().findAndRegisterModules();

        CommonCategory x = new CommonCategory("test1", new I18nString.Builder().addAsDefault("en", "Test category").build());
        String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(x);

        System.out.println(json);

        CommonCategory y = mapper.readValue(json, CommonCategory.class);
        assertThat(y.getId(), equalTo(x.getId()));
        assertThat(y.getTitle().getValue("en"), equalTo("Test category"));

    }
}