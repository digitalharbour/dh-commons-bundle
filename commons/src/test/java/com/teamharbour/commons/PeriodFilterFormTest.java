package com.teamharbour.commons;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import javax.validation.Valid;
import javax.validation.Validation;
import javax.validation.Validator;

import java.time.Clock;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.HashMap;
import java.util.Map;

import static com.teamharbour.commons.CommonsUtils.arrayToMap;
import static org.junit.jupiter.api.Assertions.*;

class PeriodFilterFormTest {

    private final ObjectMapper defaultMapper = new ObjectMapper().findAndRegisterModules();
    private final Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

    static class FilterAForm {
        public FilterAForm() {
        }

        @Valid
        @JsonProperty("fltA")
        private PeriodFilterForm fltA;

        public PeriodFilterForm getFltA() {
            return fltA;
        }
    }

    @Test
    void test_from_to_fltA() {

        Map params = new HashMap<String, Object>();
        params.put("fltA", arrayToMap("from", "2020-01-01", "to", "2020-01-02"));
        FilterAForm x = defaultMapper.convertValue(params, FilterAForm.class);

        //System.out.println(validator.validate(x));
        // object is valid and nested form as well
        assertTrue(validator.validate(x).isEmpty());
        assertTrue(validator.validate(x.fltA).isEmpty());

        assertEquals(LocalDate.of(2020, 1, 1), x.fltA.getFrom());
        assertEquals(LocalDate.of(2020, 1, 2), x.fltA.getTo());

    }

    @Test
    void test_effective_from_to_with_default_timezone() {

        PeriodFilterForm x = defaultMapper.convertValue(arrayToMap("from", "2020-01-01", "to", "2020-01-02"), PeriodFilterForm.class);
        assertTrue(validator.validate(x).isEmpty());
        assertEquals("UTC", x.getZone());

        assertEquals(Instant.parse("2020-01-01T00:00:00.000Z"), x.getEffectiveFrom());
        assertEquals(Instant.parse("2020-01-02T23:59:59.999999999Z"), x.getEffectiveTo());

    }

    @Test
    void test_effective_from_to_with_timezone() {

        PeriodFilterForm x = defaultMapper.convertValue(arrayToMap("zone", "+02:00", "from", "2020-01-01", "to", "2020-01-02"), PeriodFilterForm.class);
        assertTrue(validator.validate(x).isEmpty());
        assertEquals("+02:00", x.getZone());

        assertEquals(Instant.parse("2019-12-31T22:00:00.000Z"), x.getEffectiveFrom());
        assertEquals(Instant.parse("2020-01-02T21:59:59.999999999Z"), x.getEffectiveTo());

    }

    @Test
    void test_two_concurrent_period_methods() {

        PeriodFilterForm x = defaultMapper.convertValue(arrayToMap("from", "2020-01-01", "to", "2020-01-02", "lastWeeks", "2"), PeriodFilterForm.class);
        System.out.println(validator.validate(x));
        assertEquals(1, validator.validate(x).size());

    }

    @Test
    void test_last_week_with_zone_shifting() {

        // 23:30 in London means next day Thu 2nd Jan 01:30 in Kyiv (+02:00)
        Clock fixedClock = Clock.fixed(Instant.parse("2020-01-01T23:30:00.000Z"), ZoneId.of("+02:00"));

        PeriodFilterForm x = defaultMapper.convertValue(arrayToMap("lastWeeks", "1", "zone", "+02:00"), PeriodFilterForm.class);
        x.withClock(fixedClock); // mocking clock!!!

        assertTrue(validator.validate(x).isEmpty());

        // 00:00 of last Monday 30th in Kyiv is 22:00 of Sun 29th in London
        assertEquals(Instant.parse("2019-12-29T22:00:00Z"), x.getEffectiveFrom());
        // "today" in Kiev we have 2nd Jan, so 2nd Jan 23:59 in Kyiv is 21:59 of in London, but 2nd Jan! not 1st!
        assertEquals(Instant.parse("2020-01-02T21:59:59.999999999Z"), x.getEffectiveTo());

        System.out.println(x.getEffectiveFrom() + " -> " + x.getEffectiveTo());
        // 2019-12-29T22:00:00Z -> 2020-01-02T21:59:59.999999999Z

        // now checking 3 weeks
        x = defaultMapper.convertValue(arrayToMap("lastWeeks", "3", "zone", "+02:00"), PeriodFilterForm.class);
        x.withClock(fixedClock); // mocking clock!!!

        // three weeks ago Monday (-2 hours = Sunday) must be here
        assertEquals(Instant.parse("2019-12-15T22:00:00Z"), x.getEffectiveFrom());
        assertEquals(Instant.parse("2020-01-02T21:59:59.999999999Z"), x.getEffectiveTo()); // today date is same

        System.out.println(x);

    }

    // TODO: test lastMonth, oneMonth, oneWeek strategies

}