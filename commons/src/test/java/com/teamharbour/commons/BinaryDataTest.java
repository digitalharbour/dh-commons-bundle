package com.teamharbour.commons;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.io.CharStreams;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class BinaryDataTest {

    // http://png-pixel.com/
    private static final String transparentPixelBase64 = "iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII=";

    @Test
    public void toAndFromJson() throws IOException {

        ObjectMapper mapper = new ObjectMapper().findAndRegisterModules();

        BinaryData x = new BinaryData(transparentPixelBase64, "img/png", "t.png", null);
        String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(x);
        System.out.println(json); // visual control

        BinaryData y = mapper.readValue(json, BinaryData.class);

        assertThat(y.getBase64Data(), equalTo(x.getBase64Data()));
        assertThat(y.getContentType(), equalTo(x.getContentType()));
        assertThat(y.getFileName().get(), equalTo(x.getFileName().get()));
        assertThat(y.getContentType().isEmpty(), equalTo(x.getContentType().isEmpty()));

    }

    @Test
    void test_builder() throws IOException {

        ByteArrayInputStream stream = new ByteArrayInputStream("test".getBytes());

        BinaryData data = BinaryData
                .fromStream(stream, stream.available())
                .contentType("text/plain")
                .fileName("test.txt")
                .textCharset("UTF-8")
                .build();

        assertEquals("test".getBytes().length, data.getSize());
        assertEquals("text/plain", data.getContentType());
        assertEquals("test.txt", data.getFileName().orElse("N/A"));
        assertEquals("UTF-8", data.getTextCharset().orElse("N/A"));
        assertEquals("test", CharStreams.toString(new InputStreamReader(data.getDataStream(), data.getTextCharset().orElse("N/A"))));

        data = BinaryData.fromStats("/12345/67890", 1024)
                .contentType("text/plain")
                .build();
        assertEquals("/12345/67890", data.getDataURI());

        assertThrows(IllegalStateException.class, () -> BinaryData.fromStats("/12345/67890", 1024)
                .dataURI("again!")
                .build(),
                "Unable set dataURI twice");

    }
}