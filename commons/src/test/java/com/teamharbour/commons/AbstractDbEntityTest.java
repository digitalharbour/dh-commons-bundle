package com.teamharbour.commons;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Charsets;
import com.google.common.io.CharStreams;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStreamReader;
import java.time.Instant;

import static org.hamcrest.MatcherAssert.assertThat;

public class AbstractDbEntityTest {

    private static class MockEntity extends AbstractDbEntity {
    }

    private String json1;
    private MockEntity entity1;

    @BeforeEach
    public void setUp() throws Exception {
        json1 = CharStreams.toString(new InputStreamReader(this.getClass().getResourceAsStream("mock-entity1.json"), Charsets.UTF_8));
    }

    @Test
    public void testJsonSerialization() throws IOException {
        ObjectMapper mapper = new ObjectMapper().findAndRegisterModules();

//        System.out.println( new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").parse("2018-01-31T12:51:55.123") );
//        System.out.println( LocalDateTime.parse("2018-01-31T12:51:55", DateTimeFormatter.ISO_DATE_TIME) );
//        System.out.println( LocalDateTime.parse("2018-01-31T12:51", DateTimeFormatter.ISO_DATE_TIME) );
//fail:        System.out.println( LocalDateTime.parse("2018-01-31T12", DateTimeFormatter.ISO_DATE_TIME) );
//fail:        System.out.println( LocalDateTime.parse("2018-01-31", DateTimeFormatter.ISO_DATE_TIME) );
//
        // testing deserialization first
        entity1 = mapper.readValue(json1, MockEntity.class);

        //System.out.println(mapper.readTree(json1).get("updatedAt"));
        //System.out.println(entity1.getUpdatedAt().getTime());

        assertThat(entity1.getUpdatedAt(), CoreMatchers.equalTo(Instant.parse("2018-01-31T12:50:55.999Z")));
        assertThat(entity1.getCreatedAt(), CoreMatchers.equalTo(Instant.parse("2018-01-31T12:50:50Z")));
        assertThat(entity1.getId(), CoreMatchers.equalTo("111"));

        // testing serialization and deserialization back
        //String dateStr = LocalDateTime.ofInstant(new Date().toInstant(), ZoneOffset.UTC).format(DateTimeFormatter.ISO_DATE_TIME);

        String tmp = mapper.writeValueAsString(entity1);
        System.out.println(tmp);
        entity1 = mapper.readValue(tmp, MockEntity.class);
        assertThat(entity1.getUpdatedAt(), CoreMatchers.equalTo(Instant.parse("2018-01-31T12:50:55.999Z") ));
        assertThat(entity1.getCreatedAt(), CoreMatchers.equalTo(Instant.parse("2018-01-31T12:50:50.000Z") ));
        assertThat(entity1.getId(), CoreMatchers.equalTo("111"));
    }
}