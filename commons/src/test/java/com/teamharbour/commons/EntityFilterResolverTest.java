package com.teamharbour.commons;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.teamharbour.commons.exceptions.BadParamUserException;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;

class EntityFilterResolverTest {

    @Test
    void testBasicHashResolve() throws BadParamUserException {
        Map<String, String> params = CommonsUtils.arrayToMap(
                "page", "3",
                "limit", "100",
                "query", "foo",
                "specialMode", "aggressive"
        );

        EntityFilter x = new EntityFilterResolver().resolve(params, EntityFilter.class);

        assertThat(x.getQuery(), equalTo("foo"));
        assertThat(x.getPagingOptions(), notNullValue());
        assertThat(x.getPagingOptions().getPage(), equalTo(3L));
        assertThat(x.getPagingOptions().getLimit(), equalTo(100));
        assertThat(x.getPagingOptions().getOffset(), equalTo(200L));
        assertThat(x.getAttributes(), notNullValue());
        assertThat(x.getAttributes().get("specialMode"), equalTo("aggressive"));
    }

    @Test
    void testCreatedFromTo() throws BadParamUserException, JsonProcessingException {
        Map<String, String> params = CommonsUtils.arrayToMap(
                "page", "3",
                "limit", "100",
                "query", "foo",
                "specialMode", "aggressive",
                "createdFrom", "2017-01-01",
                "createdTo", "2017-01-01",
                "timeZone", "UTC"
        );

        EntityFilter x = new EntityFilterResolver().resolve(params, EntityFilter.class);
        System.out.println(new ObjectMapper().findAndRegisterModules().writerWithDefaultPrettyPrinter().writeValueAsString(x)); // visual control


        assertThat("Entity with 12:00 is ok for from-to same day", x.acceptedByCreatedRestriction(
                new SomeEntity("12345", CommonsUtils.parseAsDateOrDateTime(ZoneOffset.UTC, "2017-01-01T12:00"))
        ), equalTo(true));

        // previous and next day are not ok
        assertThat("Entity with next day-start is not ok", x.acceptedByCreatedRestriction(
                new SomeEntity("12345", CommonsUtils.parseAsDateOrDateTime(ZoneOffset.UTC,"2017-01-02"))
        ), equalTo(false));


        assertThat("Entity with prev day-end is not ok", x.acceptedByCreatedRestriction(
                new SomeEntity("12345", CommonsUtils.parseAsDateOrDateTime(ZoneOffset.UTC,"2016-12-31T23:59:59.999"))
        ), equalTo(false));

    }

    @Test
    // TODO: finalize filter timezone issue and finish test
    @Disabled("Not finished yet, entity filter has problems with timezones")
    void testCreatedLastDays() throws BadParamUserException, JsonProcessingException {
        Map<String, String> params = CommonsUtils.arrayToMap(
                "page", "3",
                "limit", "100",
                "createdLastDays", "1"
        );

        EntityFilter x = new EntityFilterResolver().resolve(params, EntityFilter.class);
        System.out.println(new ObjectMapper().findAndRegisterModules().writerWithDefaultPrettyPrinter().writeValueAsString(x)); // visual control

        assertThat("Entity with today is ok", x.acceptedByCreatedRestriction(
                new SomeEntity("12345", Instant.now())
        ), equalTo(true));

        assertThat("Entity yesterday is not ok", x.acceptedByCreatedRestriction(
                new SomeEntity("12345", Instant.now().minus(48, ChronoUnit.HOURS))
        ), equalTo(false));

    }

    @Test
    // TODO: finalize filter timezone issue and finish test
    @Disabled("Not finished yet, entity filter has problems with timezones")
    public void testCreatedLastDaysWithTimezone() throws BadParamUserException, JsonProcessingException {
        Map<String, String> params = CommonsUtils.arrayToMap(
                "page", "3",
                "limit", "100",
                "createdLastDays", "1",
                "timeZone", "+03:00" // supported are: ZoneOffset.SHORT_IDS and like "+02:00", "-02", "+4", "Europe/Kiev"
        );

        // just to demonstrate formats here...
        ZoneId est = ZoneOffset.of("EST", ZoneOffset.SHORT_IDS);
        ZoneId est2 = ZoneOffset.of("+03:00", ZoneOffset.SHORT_IDS);
        ZoneId est3 = ZoneOffset.of("-01:00", ZoneOffset.SHORT_IDS);
        ZoneId est4 = ZoneOffset.of("+04:00", ZoneOffset.SHORT_IDS);
        ZoneId est5 = ZoneOffset.of("+05", ZoneOffset.SHORT_IDS);
        ZoneId est6 = ZoneOffset.of("+5", ZoneOffset.SHORT_IDS);
        ZoneId est7 = ZoneOffset.of("Europe/Kiev", ZoneOffset.SHORT_IDS);

        EntityFilter x = new EntityFilterResolver().resolve(params, EntityFilter.class);
        System.out.println(new ObjectMapper().findAndRegisterModules().writerWithDefaultPrettyPrinter().writeValueAsString(x)); // visual control

        assertThat("Entity with today is ok", x.acceptedByCreatedRestriction(
                new SomeEntity("12345", Instant.now())
        ), equalTo(true));

        ZonedDateTime nowZoned = Instant.now().atZone(ZoneOffset.of("+03:00"));

        System.out.println(nowZoned.minus(24L, ChronoUnit.HOURS).toInstant());
        System.out.println(nowZoned.minus(25L, ChronoUnit.HOURS).toInstant());

        assertThat("Now (with +03:00) minus 48 hours is not ok", x.acceptedByCreatedRestriction(
                new SomeEntity("12345", nowZoned.minus(48L, ChronoUnit.HOURS).toInstant())
        ), equalTo(false));

        assertThat("Now (with +03:00) minus 24 is still ok", x.acceptedByCreatedRestriction(
                new SomeEntity("12345", nowZoned.minus(24L, ChronoUnit.HOURS).toInstant())
        ), equalTo(true));

    }

    static class SomeEntity extends AbstractEntity {

        SomeEntity(String id, Instant createdAt) {
            super(id, createdAt);
        }
    }
}