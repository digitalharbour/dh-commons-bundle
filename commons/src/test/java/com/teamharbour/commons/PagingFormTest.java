package com.teamharbour.commons;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import javax.validation.Validation;
import javax.validation.Validator;

import static com.teamharbour.commons.CommonsUtils.arrayToMap;
import static org.junit.jupiter.api.Assertions.*;

class PagingFormTest {

    private final ObjectMapper defaultMapper = new ObjectMapper();
    private final Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

    @Test
    void test_paging_from_page_and_limit() {

        PagingForm x = defaultMapper.convertValue(arrayToMap("page", "2", "limit", "25"), PagingForm.class);
        // object is valid
        assertTrue(validator.validate(x).isEmpty());

        assertEquals(2L, x.getPage().longValue());
        assertEquals(25, x.getLimit().intValue());

        x = defaultMapper.convertValue(arrayToMap("limit", "10"), PagingForm.class);
        // object is valid, default page is 1
        assertTrue(validator.validate(x).isEmpty());
        assertEquals(1L, x.getPage().longValue());
        assertEquals(10, x.getLimit().intValue());

        x = defaultMapper.convertValue(arrayToMap("page", "2"), PagingForm.class);
        // object is valid, default limit is 1
        assertTrue(validator.validate(x).isEmpty());
        assertEquals(PagingForm.DEFAULT_LIMIT, x.getLimit().intValue());

        x = defaultMapper.convertValue(arrayToMap(), PagingForm.class);
        // object is valid, default limit is 10, page is 1
        assertTrue(validator.validate(x).isEmpty());
        assertEquals(1L, x.getPage().longValue());
        assertEquals(PagingForm.DEFAULT_LIMIT, x.getLimit().intValue());

    }

    @Test
    void test_invalid_page_and_limit() {

        PagingForm x = defaultMapper.convertValue(arrayToMap("page", "-1", "limit", "10"), PagingForm.class);
        // object is invalid due negative page
        assertEquals(1, validator.validate(x).size());

        x = defaultMapper.convertValue(arrayToMap("page", "0", "limit", "10"), PagingForm.class);
        // object is invalid due zero page
        assertEquals(1, validator.validate(x).size());

        x = defaultMapper.convertValue(arrayToMap("page", "1", "limit", "0"), PagingForm.class);
        // object is invalid due zero limit
        assertEquals(1, validator.validate(x).size());

        x = defaultMapper.convertValue(arrayToMap("page", "1", "limit", "-10"), PagingForm.class);
        // object is invalid due negative limit
        assertEquals(1, validator.validate(x).size());

        x = defaultMapper.convertValue(arrayToMap("page", "0", "limit", "0"), PagingForm.class);
        // object is invalid due both page and limit
        assertEquals(2, validator.validate(x).size());

    }


}