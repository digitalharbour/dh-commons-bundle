package com.teamharbour.commons;

import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.time.ZoneId;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class CommonsUtilsDateTest {

    private static final String DATE_STR = "2017-01-01";
    // used https://www.epochconverter.com/ to get 2017-01-01 00:00 and 23:59:59.999
    private static final long DATE_TIMESTAMP_DAY_END = 1483307999999L;
    private static final long DATE_TIMESTAMP_DAY_START = 1483221600000L;


    @Test
    public void parseAsDateOrDateTime() {

        Instant x = CommonsUtils.parseAsDateOrDateTime(ZoneId.of("Europe/Kiev"), DATE_STR, true);
        assertThat("Date rolled up to end of day", x.toEpochMilli() , equalTo(DATE_TIMESTAMP_DAY_END));

        x = CommonsUtils.parseAsDateOrDateTime(ZoneId.of("Europe/Kiev"), DATE_STR, false);
        assertThat("Date rolled down to begin of day", x.toEpochMilli(), equalTo(DATE_TIMESTAMP_DAY_START));

    }
}