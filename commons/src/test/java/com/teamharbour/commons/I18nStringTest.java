package com.teamharbour.commons;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class I18nStringTest {

    @Test
    public void testIsValidISOLanguage() {
        assertThat(I18nString.isValidISOLanguage("en"), equalTo(true));
        assertThat(I18nString.isValidISOLanguage("uk"), equalTo(true));
        assertThat(I18nString.isValidISOLanguage("de"), equalTo(true));
        assertThat(I18nString.isValidISOLanguage("xx"), equalTo(false));
        assertThat(I18nString.isValidISOLanguage("xxx"), equalTo(false));
        assertThat(I18nString.isValidISOLanguage("abcd"), equalTo(false));
        assertThat(I18nString.isValidISOLanguage("DE"), equalTo(false));
        assertThat(I18nString.isValidISOLanguage("eng"), equalTo(false));
    }

    @Test
    public void testIsValidISOCountry() {
        // are OK
        assertThat(I18nString.isValidISOCountry("UA"), equalTo(true));
        assertThat(I18nString.isValidISOCountry("US"), equalTo(true));
        assertThat(I18nString.isValidISOCountry("GB"), equalTo(true));
        assertThat(I18nString.isValidISOCountry("DE"), equalTo(true));

        // are NOT ok
        assertThat(I18nString.isValidISOCountry("GER"), equalTo(false));
        assertThat(I18nString.isValidISOCountry("ua"), equalTo(false));

    }

    @Test
    public void testBuildingStrings() {
        // one name is same as default name
        assertThat(new I18nString.Builder()
                .add("en", "Name")
                .build().toString(), equalTo("Name"));

        // first name is default
        assertThat(new I18nString.Builder()
                .add("en", "Name")
                .add("uk", "Ім'я")
                .build().toString(), equalTo("Name"));

        // if default set: exactly default
        assertThat(new I18nString.Builder()
                .add("en", "Name")
                .addAsDefault("es", "Nombre")
                .add("uk", "Ім'я")
                .build().toString(), equalTo("Nombre"));
    }

    @Test
    public void testJson() throws IOException {
        I18nString a = new I18nString.Builder()
                .add("en", "Name")
                .addAsDefault("es", "Nombre")
                .add("uk", "Ім'я")
                .build();

        ObjectMapper mapper = new ObjectMapper().findAndRegisterModules();
        String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(a);
        System.out.println(json); // visual control

        I18nString b = mapper.readValue(json, I18nString.class);
        assertThat(b.getValue("en"), equalTo(a.getValue("en")));
        assertThat(b, equalTo(a));

        a = new I18nString.Builder()
                .add("en", "Name")
                .build();
        System.out.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(a));

        String noDefaultValueJson = "{\"values\": {\"en\": \"English\", \"de\": \"Deutsch\"}}";
        b = mapper.readValue(noDefaultValueJson, I18nString.class);
        assertThat(b.getDefaultLang(), equalTo("de"));
        assertThat(b.getValue("en"), equalTo("English"));
        System.out.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(b));
    }
}