package com.teamharbour.commons;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.*;

public class PagingOptionsTest {

    private PagingOptions tst;

    @Test
    void jsonTest() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper().findAndRegisterModules();
        tst = PagingOptions.createOffsetAndLimit(10, 100);

        System.out.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(tst));

        tst = PagingOptions.createPageAndLimit(2, 10);

        System.out.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(tst));

    }

    @Test
    public void createOffsetAndLimit() {
        tst = PagingOptions.createOffsetAndLimit(0, 100);
        assertThat(tst.getOffset(), CoreMatchers.equalTo(0L));
        assertThat(tst.getLimit(), CoreMatchers.equalTo(100));
    }

    @Test
    public void createPageAndLimit() {
        try {
            tst = PagingOptions.createPageAndLimit(0, 100);
            fail("IllegalArgumentException not thrown");
        }
        catch (IllegalArgumentException e) {
            assertTrue(true);
        }
    }

    @Test
    public void getPage() {

        tst = PagingOptions.createOffsetAndLimit(10, 100);
        assertEquals(1, tst.getPage(), "Page auto-detected from offset and limit as 1st if offset < limit");

        tst = PagingOptions.createOffsetAndLimit(10, 10);
        assertEquals(2, tst.getPage(), "Page auto-detected from offset and limit as 2nd if offset=limit");

        tst.updatePage(2);
        assertEquals(2, tst.getPage(), "But page appeared when updating it (based on limit as page size)");

    }

    @Test
    public void updatePage() {
        tst = PagingOptions.createOffsetAndLimit(10,100);
        tst.updatePage(4);
        assertThat(tst.getOffset(), CoreMatchers.equalTo(300L));
        tst.updatePage(1);
        assertThat(tst.getOffset(), CoreMatchers.equalTo(0L));


        try {
            tst.updatePage(0);
            fail("Unable to set page to 0");
        } catch (IllegalArgumentException e) {
            assertTrue(true);
        }

        try {
            tst.updatePage(-2);
            fail("Unable to set page to negative value");
        } catch (Exception e) {
            assertTrue(true);
        }

    }

    @Test
    void to_string_is_ok() {
        tst = PagingOptions.createOffsetAndLimit(10,100);
        assertFalse(tst.toString().contains("page"));
        System.out.println(tst.toString());

        tst = PagingOptions.createPageAndLimit(3,100);
        assertFalse(tst.toString().contains("offset"));
        System.out.println(tst.toString());

    }
}