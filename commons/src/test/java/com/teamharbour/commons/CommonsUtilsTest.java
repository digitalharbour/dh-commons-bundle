package com.teamharbour.commons;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

import static com.teamharbour.commons.CommonsUtils.formatMessage;
import static com.teamharbour.commons.CommonsUtils.mapToValuesArray;
import static java.util.Collections.emptyMap;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.*;

public class CommonsUtilsTest {

    @Test
    public void testMaskString() {
        assertEquals("int...tion", CommonsUtils.maskString("internationalization", 3, 4));
        assertEquals("in", CommonsUtils.maskString("in", 1, 1));
        assertEquals("i...n", CommonsUtils.maskString("ivan", 1, 1));
        assertEquals("...", CommonsUtils.maskString("ivan", 0, 0));
        assertEquals("inter...", CommonsUtils.maskString("internationalization", 5, 0));
        assertEquals("...tion", CommonsUtils.maskString("internationalization", 0, 4));
    }

    @Test
    public void testMaskStringBigFirstChars() {
        assertThat("Big first chars must maximize content", CommonsUtils.maskString("ivan", 100, 0), equalTo("ivan"));
    }

    @Test
    public void testMaskNullString() {
        assertThat("Null gives just null", CommonsUtils.maskString(null, 10, 10), equalTo("null"));
    }

    @Test
    public void testMapToValuesArray() {
        Map<String, String> map = new LinkedHashMap<>(); // linked to keep order of placed elements
        map.put("k2", "v2");
        map.put("k3", "v3");
        map.put("k1", "v1"); // not sorted by key!

        String[] x = mapToValuesArray(map);

        System.out.println(map + " vs " + String.join(",", x));

        assertArrayEquals(new String[]{"v2", "v3", "v1"}, x, "To list is sorting values by putting order");
    }

    @Test
    void testFormatMessage() {
        String pattern = "Param {} has value {} but not {}";

        String x = formatMessage(pattern, "x", "1", "2");
        assertEquals("Param x has value 1 but not 2", x);

        // test if reliable about user mistake tolerance about number of params (less)
        x = formatMessage(pattern, "x", "1");
        assertEquals("Param x has value 1 but not {}", x);

        x = formatMessage(pattern, "x", "1", "2", "3");
        assertEquals("Param x has value 1 but not 2", x);

        x = formatMessage("No placeholders", "1", "2");
        assertEquals("No placeholders", x);

        x = formatMessage("", "1", "2");
        assertEquals("", x);

        x = formatMessage("");
        assertEquals("", x);

        x = formatMessage("Null this {}", new String[]{null});
        assertEquals("Null this null", x);

    }

    @Test
    public void testNullMapToValuesArray() {
        assertThrows(NullPointerException.class, ()->{
            mapToValuesArray(null);
        });
    }

    @Test
    public void testEmptyMapToValuesArray() {
        String[] emptyArray = new String[0];
        assertArrayEquals(emptyArray, mapToValuesArray(emptyMap()));
    }

    @Test
    void testArrayToMap() {
        Map m = CommonsUtils.arrayToMap("par9", "val9", "par1", "val1", "par2", "val2");
        assertThat(m, notNullValue());
        assertThat(m.get("par1"), CoreMatchers.equalTo("val1"));
        assertThat(m.get("par2"), CoreMatchers.equalTo("val2"));

        assertThrows(IllegalArgumentException.class,
                () -> CommonsUtils.arrayToMap("par1", "val1", "par2"),
                "Odd parameters count is deprecated"
        );


        Iterator i = m.values().iterator();
        assertEquals("val9", i.next(), "val9 must go first cause added first");
        assertEquals("val1", i.next(), "val1 is next to val9");
        assertEquals("val2", i.next(), "val2 is next to val1");

    }

    @Test
    void testLoadGitInfo() throws JsonProcessingException {
        GitInfo gitInfo;

        gitInfo = CommonsUtils.loadGitInfo(CommonsUtilsTest.class, "/git-empty.properties");
        assertNull(gitInfo.build.version, "Empty git info gives empty build version");

        assertThrows(IllegalStateException.class, () -> {
            CommonsUtils.loadGitInfo(CommonsUtilsTest.class, "/git-NON-EXISTING.properties");
        }, "Throws runtime on non-existing resource");

        gitInfo = CommonsUtils.loadGitInfo(CommonsUtilsTest.class, "/git-full.properties");
        // visual control
        System.out.println(new ObjectMapper().findAndRegisterModules().writerWithDefaultPrettyPrinter().writeValueAsString(gitInfo));
        assertEquals("0.8.0", gitInfo.build.version);
        assertEquals("67691ca687a2766c7e87949cb1eb7a251112b842", gitInfo.getCommitId());
    }

    @Test
    void test_stacktrace_printed() {
        Logger l = LoggerFactory.getLogger(this.getClass());
        assertNotNull(l); /// just for Sonar Lint

        l.info("Test {}, {}, {}", 1, 2, 3, 4, new Exception("Will be stacktrace here?"));
    }
}
