package com.teamharbour.commons;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import static java.lang.String.format;

/**
 * Catalog model for tree structures based on categories (folders) and items.
 * Categories could have parent category, child category.
 * @param <C> class for categories
 * @param <I> class for items
 */
@JsonInclude(JsonInclude.Include.NON_ABSENT)
public class Catalog<C, I> {

    private Node<C, I> tree;
    private Walker currentWalker;

    public Catalog() {
        this.tree = new Node<>(null, null, null, null);
    }

    public Walker newWalker() {
        return new Walker<>(this);
    }

    public Walker walker() {
        if (this.currentWalker == null)
            this.currentWalker = this.newWalker();
        return this.currentWalker;
    }

    public Node<C, I> getTree() {
        return tree;
    }

    public static class Walker<T extends Catalog<C,I>, C, I> {

        private Node<C,I> currentCategory;
        private I currentItem;
        private T catalog;

        public Walker(T catalog) {
            this.catalog = catalog;
            this.currentCategory = catalog.getTree(); // link the root node as current
            this.updateCurrentItem(); // set current item if it is already here
        }

        public T getCatalog() {
            return catalog;
        }

        public C getCategory() {
            if (this.currentCategory.isRoot())
                throw new IllegalStateException("Unable to category object cause Walker is at root now");
            return this.currentCategory.category;
        }

        public boolean isAtRoot() {
            return this.currentCategory.isRoot();
        }

        /**
         * @return unmodifiable list of items
         */
        public List<I> getItems() {
            return Collections.unmodifiableList(this.currentCategory.items);
        }

        /**
         * Adds child category into current category
         * @param category category to add
         */
        public Walker<T, C, I> addCategory(C category) {
            Node<C, I> prev = null;
            if (!this.currentCategory.children.isEmpty())
                prev = this.currentCategory.children.peekLast();
            this.currentCategory.children.add(new Node<>(this.currentCategory, prev, null, category));
            return this;
        }

        /**
         * Adds child item into current category
         * @param item item to add
         * @return current walker instance
         */
        public Walker<T, C, I> addItem(I item) {
            this.currentCategory.items.add(item);
            this.currentItem = item;
            return this;
        }

        public Walker<T, C, I> down() {
            if (this.currentCategory.children.isEmpty())
                throw new IllegalStateException(format("Unable to go down cause no children for node '%s'", this.currentCategory.category));
            this.currentCategory = this.currentCategory.children.peekFirst();
            updateCurrentItem();
            return this;
        }

        public Walker<T, C, I> up() {
            if (this.currentCategory.isRoot())
                throw new IllegalStateException("Unable to go up cause Walker is already at the root");
            this.currentCategory = this.currentCategory.parent;
            updateCurrentItem();
            return this;
        }

        public Walker<T, C, I> next() {
            if (this.currentCategory.next == null)
                throw new IllegalStateException("Unable to go next cause Walker is already at the last category");
            this.currentCategory = this.currentCategory.next;
            updateCurrentItem();
            return this;
        }

        public Walker<T, C, I> prev() {
            if (this.currentCategory.prev == null)
                throw new IllegalStateException("Unable to go prev cause Walker is already at the first category");
            this.currentCategory = this.currentCategory.prev;
            updateCurrentItem();
            return this;
        }

        private void updateCurrentItem() {
            // setting current item to first item of new current category
            if (!this.currentCategory.items.isEmpty())
                this.currentItem = this.currentCategory.items.get(0);
            else
                this.currentItem = null;
        }

        public I getItem() {
            return this.currentItem;
        }

        public List<C> getSubCategories() {
            return Collections.unmodifiableList(
                    this.currentCategory.children.stream()
                            .map(x -> x.category)
                            .collect(   Collectors.toList() )
            );
        }

        public Walker<T, C, I> root() {
            this.currentCategory = this.catalog.getTree();
            updateCurrentItem();
            return this;
        }
    }

    @JsonInclude(JsonInclude.Include.NON_ABSENT)
    private static class Node<C, I> {
        private Node<C, I> parent;
        private C category;
        private LinkedList<Node<C, I>> children;
        private LinkedList<I> items;
        private Node<C, I> next;
        private Node<C, I> prev;

        public Node() {
        }

        private Node(Node<C, I> parent, Node<C, I> prev, Node<C, I> next, C category) {
            this.parent = parent;
            this.prev = prev;
            this.next = next;
            this.category = category;
            this.children = new LinkedList<>();
            this.items = new LinkedList<>();
            if (prev != null)
                prev.next = this;
            if (next != null)
                next.prev = this;
        }

        public C getCategory() {
            return category;
        }

        public List<Node<C, I>> getChildren() {
            return Collections.unmodifiableList(children);
        }

        public List<I> getItems() {
            return Collections.unmodifiableList(items);
        }

        @JsonIgnore
        public boolean isRoot() {
            return this.parent == null;
        }

    }
}
