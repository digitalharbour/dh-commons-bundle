package com.teamharbour.commons;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.InputStream;
import java.util.Base64;
import java.util.Optional;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Most common binary content for transferring, storing and providing to customer in appropriate way
 * (having content-type, recommended filename for storing, charset if it is text content)
 */
@JsonInclude(JsonInclude.Include.NON_ABSENT)
public class BinaryData {

    /**
     * @deprecated use data URI and/or dataStream
     */
    @Deprecated
    private String base64Data;

    /**
     * data stream provided
     */
    @JsonIgnore
    private InputStream dataStream;

    /**
     * Abstract URI string, which may content any kind of data location URL, e.g. UUID in S3 server.
     * As worst case (not recommended but ok for small files) it could be string like: "data:image/png;base64,base64BodyHere"
     */
    private String dataURI;

    private String contentType;

    private String fileName;

    private String textCharset;

    private Long size;

    /**
     * @deprecated use BinaryData() and then setters you need
     */
    @Deprecated
    @JsonCreator(mode = JsonCreator.Mode.PROPERTIES)
    public BinaryData(
            @JsonProperty("base64Data") String base64Data,
            @JsonProperty("contentType") String contentType,
            @JsonProperty("fileName") String fileName,
            @JsonProperty("textCharset") String textCharset) {
        this.base64Data = base64Data;
        this.contentType = contentType;
        this.fileName = fileName;
        this.textCharset = textCharset;
    }

    public BinaryData() {
        // simple bean constructor
    }

    /**
     * Minimal mandatory args constructor
     * @param dataURI data URI (ID, URL or something pointing on real binary data; base64 on worst case or if file size is small)
     * @param contentType content-type
     */
    public BinaryData(String dataURI, String contentType) {
        this.dataURI = dataURI;
        this.contentType = contentType;
    }

    public static Builder fromStream(InputStream stream, long size) {
        return new Builder(stream, size);
    }

    public static Builder fromStats(String dataURI, long size) {
        return new Builder(dataURI, size);
    }

    public void setDataStream(InputStream dataStream) {
        this.dataStream = dataStream;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public void setFileName(String fileName) {
        this.fileName = checkNotNull(fileName);
    }

    public void setTextCharset(String textCharset) {
        this.textCharset = checkNotNull(textCharset);
    }

    public void setDataURI(String dataURI) {
        this.dataURI = dataURI;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public InputStream getDataStream() {
        return dataStream;
    }

    public String getDataURI() {
        return dataURI;
    }

    /**
     * @deprecated use data URI or/and #getDataStream()
     * @return the most compact and useful binary encoding for data
     */
    @Deprecated
    public String getBase64Data() {
        return base64Data;
    }

    /**
     * @deprecated use data URI
     * @return returns binary data
     */
    @Deprecated
    @JsonIgnore
    public byte[] getBinaryData() {
        return Base64.getDecoder().decode(this.base64Data);
    }

    /**
     * @return content-type for choosing better application to display content to end-user
     */
    public String getContentType() {
        return contentType;
    }

    public Long getSize() {
        return size;
    }

    /**
     * @return optional file name, use if need some default file name when end-user may wanna store it as file
     */
    public Optional<String> getFileName() {
        return Optional.ofNullable(fileName);
    }

    public Optional<String> getTextCharset() {
        return Optional.ofNullable(textCharset);
    }

    @Override
    public String toString() {
        return "BinaryData{" +
                "dataURI='" + dataURI + '\'' +
                ", contentType='" + contentType + '\'' +
                ", fileName='" + fileName + '\'' +
                ", size=" + size +
                '}';
    }

    public static class Builder {

        private InputStream data;
        private final long size;
        private String contentType;
        private String fileName;
        private String charset;
        private String dataURI;
        private final boolean statsOnly;

        public Builder(InputStream stream, long size) {
            this.data = stream;
            this.size = size;
            this.statsOnly = false;
        }

        public Builder(String dataURI, long size) {
            this.dataURI = dataURI;
            this.size = size;
            this.statsOnly = true;
        }

        public Builder contentType(String contentType) {
            this.contentType = contentType;
            return this;
        }

        public Builder fileName(String fileName) {
            this.fileName = fileName;
            return this;
        }

        public Builder textCharset(String charset) {
            this.charset = charset;
            return this;
        }

        public Builder dataURI(String dataURI) {
            if (this.statsOnly)
                throw new IllegalStateException("dataURI already set for stats-only BinaryData");

            this.dataURI = dataURI;
            return this;
        }

        public BinaryData build() {
            BinaryData res = new BinaryData();
            res.contentType = this.contentType;
            res.textCharset = this.charset;
            res.dataStream = this.data;
            res.dataURI = this.dataURI;
            res.fileName = this.fileName;
            res.size = this.size;
            return res;
        }

    }
}
