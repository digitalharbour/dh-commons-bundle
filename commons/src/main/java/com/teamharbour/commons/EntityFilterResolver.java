package com.teamharbour.commons;

import com.teamharbour.commons.exceptions.BadParamUserException;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;

/**
 * Helps to create EntityFilter.
 * Sets offset, limit, page and query params parameters.
 * If you need any other parameters (for some inherited from EntityFilter class),
 * override putAttribute() to initialize needed attribute from string.
 * Look at {@link EntityFilter#putAttribute(String, String)} implementation as example
 * (it initialize createdFrom, createdTo, createdLastDays options)
 *
 * @deprecated use PagingForm, SortingForm, PeriodFilter in e.g. spring controller params and/or build own custom XxxFilterFrom class combining previous
 */
 @Deprecated
public class EntityFilterResolver {

    private static final int DEFAULT_LIMIT = 100;
    public static final String PARAM_LIMIT = "limit";
    private static final String PARAM_PAGE = "page";
    public static final String PARAM_OFFSET = "offset";
    public static final String PARAM_QUERY = "query";

    private int defaultLimit;


    public EntityFilterResolver() {
        this.defaultLimit = DEFAULT_LIMIT;
    }

    public EntityFilterResolver defaultLimit(int defaultLimit) {
        this.defaultLimit = defaultLimit;
        return this;
    }

    /**
     * Creating entity filter from map, which may contain "limit", "page", "offset", "query" keys.
     * All other keys saved as putAttribute()
     * @param paramsMap parameters map, where PARAM_* will be recognized keys
     * @param clazz class of filter MUST have default constructor
     * @return resolved entity filter
     * @throws BadParamUserException if some PARAM_* is bad (limit, offset, page)
     * @throws IllegalAccessException when unable to instantiate by default constructor
     * @throws InstantiationException when unable to instantiate by default constructor
     */
    public <T extends EntityFilter> T resolve(Map<String, String> paramsMap, Class<T> clazz)
            throws BadParamUserException {

        int limit;
        long offset;
        int page;
        PagingOptions paging;

        limit = Integer.parseInt(paramsMap.getOrDefault(PARAM_LIMIT, String.valueOf(this.defaultLimit)));
        if (limit < 0) throw new BadParamUserException.BadPositiveIntegerBuilder(PARAM_LIMIT, limit).build();

        if (paramsMap.containsKey(PARAM_PAGE)) {
            page = Integer.parseInt(paramsMap.getOrDefault(PARAM_PAGE, "1")); // 1st page by default
            if (page < 1) throw new BadParamUserException.CommonBuilder(PARAM_PAGE, page).build();
            paging = PagingOptions.createPageAndLimit(page, limit);
        }
        else {
            offset = Long.parseLong(paramsMap.getOrDefault(PARAM_OFFSET, "0")); // zero offset by default
            if (offset < 0L) throw new BadParamUserException.BadPositiveIntegerBuilder(PARAM_OFFSET, offset).build();
            paging = PagingOptions.createOffsetAndLimit(offset, limit);
        }

        String query = paramsMap.getOrDefault(PARAM_QUERY, "");


        // Instantiating filter (must have default constructor)

        T filter;
        try {
            filter = clazz.getDeclaredConstructor().newInstance();
        } catch (Exception e) {
            throw new IllegalStateException("Unable to instantiate class: " + clazz, e);
        }
        filter.setPagingOptions(paging);
        filter.setQuery(query);
        paramsMap.forEach((k,v) -> {
            if (!PARAM_LIMIT.equals(k) && !PARAM_OFFSET.equals(k) && !PARAM_PAGE.equals(k) && !PARAM_QUERY.equals(k))
                filter.putAttribute(k, v);
        });

        return filter;
    }

}
