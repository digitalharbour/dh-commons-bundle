package com.teamharbour.commons.exceptions;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.teamharbour.commons.CommonsUtils;
import io.swagger.v3.oas.annotations.media.Schema;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import static com.teamharbour.commons.CommonsUtils.formatMessage;
import static com.teamharbour.commons.CommonsUtils.mapToValuesArray;

/**
 * Exception thrown to end-user.
 * It should be thrown by service-layer and if thrown once, must be delivered to end-user.
 */
@JsonInclude(JsonInclude.Include.NON_ABSENT)
@JsonIgnoreProperties({"stackTrace", "cause", "message", "localizedMessage", "suppressed"}) // this not works for Swagger ignoring!
@JsonPropertyOrder({"messageKey", "defaultMessage"})
public abstract class CommonUserException extends Exception {

    /**
     * Not authenticated. System cannot authenticate that this user is authenticated. In HTTP context, usually to 401 code.
     */
    public static final String NOT_AUTHENTICATED = "not_authenticated";

    /**
     * Not authorized. In HTTP context, usually mapped to 403 error code
     */
    public static final String NOT_AUTHORIZED = "not_authorized";

    public static final String SESSION_EXPIRED = "session_expired";

    /**
     * Some abstract item not found. In HTTP context, usually mapped to 404 error code
     */
    public static final String ITEM_NOT_FOUND = "item_not_found";

    /**
     * Indicates that some core issues.
     * Should be addressed to support team.
     * In HTTP context, usually mapped to 500 error code
     */
    public static final String UNKNOWN_SERVER_ERROR = "unknown_server_error";

    /**
     * Validation failed.
     * In HTTP context should be mapped to 400 error code.
     */
    public static final String VALIDATION_FAILED = "validation_failed";

    /**
     * Bad parameters.
     * In HTTP context, usually mapped to 400 error code
     */
    public static final String BAD_PARAMETER = "bad_parameter";
    public static final String BAD_PARAMETER_SIZE = "bad_size";
    public static final String BAD_PARAMETER_POS_INT = "bad_pos_integer";
    public static final String BAD_PARAMETER_FORMAT = "bad_format";
    public static final String BAD_PARAMETER_RANGE = "bad_range";
    public static final String BAD_PARAMETER_MISSING = "missing_parameter";
    public static final String UN_AUTHORIZED = "unauthorized";
    public static final String REST_CLIENT_PROBLEM = "rest_client_problem";
    public static final String ILLEGAL_OPERATION = "illegal_operation";


    private final String messageKey;
    private final String defaultMessage;

    private final Map<String, String> parameters = new LinkedHashMap<>();

    /**
     * @param messageKey     message key for i18n dictionary on end-user client
     * @param defaultMessage message for end-user if no message key in i18n dictionary
     */
    public CommonUserException(String messageKey, String defaultMessage) {
        super(defaultMessage);
        this.messageKey = messageKey;
        this.defaultMessage = defaultMessage;
    }

    /**
     *
     * @param messageKey    message key for i18n dictionary on end-user client
     * @param defaultMessagePattern message for end-user if no message key in i18n dictionary
     * @param params map of name=&gt;value paramsAsOddNameEvenValue for message
     */
    public CommonUserException(String messageKey, String defaultMessagePattern, Map<String, String> params) {
        this(messageKey, formatMessage(defaultMessagePattern, new ArrayList<String>(params.values()).toArray(new String[]{})) );
        this.parameters.putAll(params);
    }

    /**
     * @param messageKey    message key for i18n dictionary on end-user client
     * @param defaultMessagePattern message for end-user if no message key in i18n dictionary
     * @param paramsAsOddKeyEvenValue list of parameters as [par1name, par1value, par2name, par2value]
     *
     */
    public CommonUserException(String messageKey, String defaultMessagePattern, String... paramsAsOddKeyEvenValue) {
        this(messageKey, defaultMessagePattern, CommonsUtils.arrayToMap(paramsAsOddKeyEvenValue));
    }

    /**
     * @return message key for i18n dictionary on end-user client
     */
    @Schema(required = true, description = "Message key for using as key for multi-language GUI clients or for custom GUI error messages (to override `defaultMessage`)")
    public String getMessageKey() {
        return messageKey;
    }

    /**
     * @return message for end-user if no message key in i18n dictionary
     */
    @Schema(required = true, description = "Default message; GUI client could use it as default when no messageKey alias configured")
    public String getDefaultMessage() {
        return defaultMessage;
    }

    /**
     * @return parameters map for exact parameters matching
     */
    @Schema(required = true, description = "Map of message parameters which could be substituted into error message pattern like 'Parameter {x} is wrong' -> 'Parameter COMMENT is wrong: 'x' is parameter, 'COMMENT' is the value of parameter")
    public Map<String, String> getParametersMap() {
        return parameters;
    }

    /**
     * @return parameters as list sorted by keys
     */
    @Schema(required = true, description = "List of message parameters which could be substituted into error message pattern like 'Parameter {} is wrong cause {}' -> 'Parameter COMMENT is wrong cause EMPTY': 'COMMENT' and 'EMPTY' are parameter values")
    public String[] getParametersList() {
        return mapToValuesArray(this.parameters);
    }

    @JsonIgnore // must keep explicit override to ignore in Swagger
    @Override
    public StackTraceElement[] getStackTrace() {
        return super.getStackTrace();
    }

    @JsonIgnore // must keep explicit override to ignore in Swagger
    @Override
    public String getMessage() {
        return super.getMessage();
    }

    @JsonIgnore // must keep explicit override to ignore in Swagger
    @Override
    public String getLocalizedMessage() {
        return super.getLocalizedMessage();
    }

    @JsonIgnore // must keep explicit override to ignore in Swagger
    @Override
    public synchronized Throwable getCause() {
        return super.getCause();
    }

    @JsonIgnore // hack to remove suppressed from swagger
    public Throwable getsuppressed() {
        return null;
    }

}
