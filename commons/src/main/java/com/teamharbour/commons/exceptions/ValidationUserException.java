package com.teamharbour.commons.exceptions;

import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.ConstraintViolation;
import java.util.*;
import java.util.stream.Collectors;

/**
 * General validation exception which may contain bad params (collected during complex validation).
 * E.g. for REST APIs it should be transformed to HTTP 400 error.
 */
public class ValidationUserException extends CommonUserException {

    private final List<CommonUserException> badParams = new ArrayList<>();

    public ValidationUserException(String defaultMessage) {
        super(CommonUserException.VALIDATION_FAILED, defaultMessage);
    }

    public ValidationUserException(String messageKey, String defaultMessage) {
        super(messageKey, defaultMessage);
    }

    public ValidationUserException(String messageKey, String defaultMessagePattern, Map<String, String> messageParams) {
        super(messageKey, defaultMessagePattern, messageParams);
    }

    public ValidationUserException(String messagePattern, Map<String, String> messageParams) {
        super(CommonUserException.VALIDATION_FAILED, messagePattern, messageParams);
    }

    public ValidationUserException addConstraintViolations(Set<ConstraintViolation<Object>> violations) {
        this.badParams.addAll(violations.stream()
                .map(x -> new BadParamUserException.ConstraintViolationBuilder(x).build())
                .collect(Collectors.toList()));
        return this;
    }

    @Schema(description = "list of inherited (children) errors having same structure as any other error: messageKey, defaultMessage, etc.; used when validating forms which could have many fields")
    public List<CommonUserException> getBadParams() {
        return badParams;
    }

    public ValidationUserException addConstraintViolations(Collection<CommonUserException> badParams) {

        this.badParams.addAll(badParams);

        return this;
    }


}
