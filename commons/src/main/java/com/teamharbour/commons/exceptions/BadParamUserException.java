package com.teamharbour.commons.exceptions;

import com.teamharbour.commons.CommonsUtils;
import io.swagger.v3.oas.annotations.media.Schema;
import org.hibernate.validator.constraints.Range;

import javax.validation.ConstraintViolation;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Optional;

import static com.teamharbour.commons.CommonsUtils.arrayToMap;
import static java.lang.String.format;
import static java.lang.String.valueOf;

/**
 * End User exception delivered when classic 'bad parameter' issue happens.
 * If you need describe more than one bad parameter, use {@link ValidationUserException}
 * It is separate exception for convenient handling as HTTP 400 or other controllers working with this issue.
 */
public class BadParamUserException extends ValidationUserException {

    private final String paramName;

    /**
     * Single bad param constructor
     *
     * @param defaultMessagePattern default defaultMessageTemplate
     * @param paramName      param name
     * @param wasValue     param value
     */
    private BadParamUserException(String paramName, String wasValue, String messageKey, String defaultMessagePattern, String... messageParams) {
        super(messageKey, defaultMessagePattern, arrayToMap(messageParams));
        this.paramName = paramName;
        this.getParametersMap().put("name", paramName);
        this.getParametersMap().put("was", wasValue);
    }

    public static class ConstraintViolationBuilder extends CommonBuilder {

        private CommonBuilder effectiveBuilder;

        ConstraintViolationBuilder(ConstraintViolation<Object> x) {

            super(BAD_PARAMETER, x.getPropertyPath().toString(), x.getInvalidValue());

            if (x.getConstraintDescriptor().getAnnotation() instanceof Range) {
                Range a = (Range)x.getConstraintDescriptor().getAnnotation();
                this.effectiveBuilder = new BadParamUserException.IntegerRangeBuilder(
                        x.getPropertyPath().toString(),
                        a.min(),
                        a.max(),
                        x.getInvalidValue());
            } else if (x.getConstraintDescriptor().getAnnotation() instanceof Size) {
                Size a = (Size)x.getConstraintDescriptor().getAnnotation();
                this.effectiveBuilder = new BadParamUserException.IntegerRangeBuilder(
                        x.getPropertyPath().toString(),
                        a.min(),
                        a.max(),
                        x.getInvalidValue());
            }
            else if (x.getConstraintDescriptor().getAnnotation() instanceof Pattern) {
                Pattern a = (Pattern) x.getConstraintDescriptor().getAnnotation();
                this.effectiveBuilder = new BadParamUserException.BadFormatBuilder(
                        x.getPropertyPath().toString(),
                        a.regexp(),
                        x.getInvalidValue());
            }
            else if (x.getConstraintDescriptor().getAnnotation() instanceof NotNull) {
                this.effectiveBuilder = new BadParamUserException.CommonBuilder(
                        "must_not_be_empty",
                        x.getPropertyPath().toString(),
                        x.getInvalidValue()).defaultMessage(format("Parameter '%s' cannot be empty", x.getPropertyPath().toString()));
            }
            else {
                this.effectiveBuilder = new CommonBuilder(
                        BAD_PARAMETER,
                        x.getPropertyPath().toString(),
                        x.getInvalidValue())
                        .defaultMessage(x.getMessage());
            }

            // if message created with '_' that means it is key (do not know how to define better evidence)
            if (x.getMessage().contains("_"))
                this.effectiveBuilder.errorKey(x.getMessage());

        }

        @Override
        public BadParamUserException build() {
            return this.effectiveBuilder.build();
        }
    }

    public static class CommonBuilder {
        final String paramName;
        final String was;
        String errorKey;
        String wasKey = "was";
        String defaultMessage;

        public CommonBuilder(String paramName, Object was) {
            this(BAD_PARAMETER, paramName, was);
        }

        public CommonBuilder(String errorKey, String paramName, Object was) {
            this.errorKey = errorKey;
            this.paramName = paramName;
            this.was = CommonsUtils.maskString("" + was, 1024, 0);
        }

        public CommonBuilder defaultMessage(String defaultMessage) {
            this.defaultMessage = defaultMessage;
            return this;
        }

        CommonBuilder errorKey(String errorKey) {
            this.errorKey = errorKey;
            return this;
        }

        CommonBuilder wasKey(String wasKey) {
            this.wasKey = wasKey;
            return this;
        }

        public BadParamUserException build() {
            return new BadParamUserException(
                    this.paramName,
                    this.was,
                    this.errorKey,
                    Optional.ofNullable(this.defaultMessage).orElse(format("Bad '%s' parameter: '%s'", this.paramName, this.was)));
        }
    }

    public static class BadPositiveIntegerBuilder extends CommonBuilder {
        private String defaultMessageTemplate = "Must be integer value but not '%s'";

        public BadPositiveIntegerBuilder(String paramName, Object was) {
            super(BAD_PARAMETER_POS_INT, paramName, was);
        }

        @Override
        public BadParamUserException build() {
            return new BadParamUserException(
                    paramName,
                    was,
                    errorKey,
                    format(defaultMessageTemplate, was)
            );
        }
    }

    public static class IntegerRangeBuilder extends CommonBuilder {
        private final long min;
        private final long max;
        private String minKey = "min";
        private String maxKey = "max";
        private String defaultMessageTemplate = "Range must be between %s and %s, but not '%s'";

        public IntegerRangeBuilder(String paramName, int min, int max, Object was) {
            super(BAD_PARAMETER_SIZE, paramName, was);

            this.min = min;
            this.max = max;
        }

        public IntegerRangeBuilder(String paramName, long min, long max, Object was) {
            super(BAD_PARAMETER_SIZE, paramName, was);

            this.min = min;
            this.max = max;
        }

        IntegerRangeBuilder minMaxMessageKeys(String minKey, String maxKey, String wasKey) {
            this.minKey = minKey;
            this.maxKey = maxKey;
            wasKey(wasKey);

            return this;
        }

        @Override
        public BadParamUserException build() {
            return new BadParamUserException(
                    paramName,
                    was,
                    errorKey,
                    format(defaultMessageTemplate, this.min, this.max, this.was),
                    minKey, valueOf(min),
                    maxKey, valueOf(max),
                    wasKey, was);
        }
    }

    public static class BadFormatBuilder extends CommonBuilder {
        private final String violatedRule;
        public BadFormatBuilder(String paramName, String violatedRule, Object invalidValue) {
            super(CommonUserException.BAD_PARAMETER_FORMAT, paramName, invalidValue);
            this.violatedRule = violatedRule;
        }

        @Override
        public BadParamUserException build() {
            return new BadParamUserException(
                    paramName,
                    was,
                    errorKey,
                    format("Parameter '%s'='%s' does not correspond pattern '%s'", paramName, was, violatedRule),
                    "violatedPattern", violatedRule,
                    "was", was
            );
        }
    }

    @Schema(description = "Name of parameter which validation was failed; always duplicated into `parameterMap` and `parameterList`")
    public String getParamName() {
        return paramName;
    }
}
