package com.teamharbour.commons.exceptions;

import java.util.Map;

import static com.teamharbour.commons.CommonsUtils.arrayToMap;

/**
 * End User exception delivered when classic 'not found' issue happens.
 * It is separate exception for convenient handling as HTTP 404 or other controllers working with this issue
 */
public class NotFoundUserException extends CommonUserException {

    /**
     * Custom messages constructor
     */
    public NotFoundUserException(String messageKey, String defaultMessagePattern, String... paramsAsOddKeyEvenValue) {
        this(messageKey, defaultMessagePattern, arrayToMap(paramsAsOddKeyEvenValue));
    }

    /**
     * Custom message and params constructor
     */
    public NotFoundUserException(String messageKey, String defaultMessagePattern, Map<String,String> params) {
        super(messageKey, defaultMessagePattern, params);
    }

    /**
     * Helper constructor when not found item type and ID are considered
     * @param itemTypeNotFound not found item type
     * @param itemIdNotFound not found item id
     */
    public NotFoundUserException(String itemTypeNotFound, String itemIdNotFound) {
        this(
                ITEM_NOT_FOUND,
                "Item '" + itemTypeNotFound + ":" + itemIdNotFound + "' not found",
                arrayToMap("id", itemIdNotFound, "type", itemTypeNotFound)
        );
    }

    /**
     * Helper constructor when not found item type could be class
     * @param itemClassNotFound class of not found item
     * @param itemIdNotFound id of not found item
     */
    public NotFoundUserException(Class itemClassNotFound, String itemIdNotFound) {
        this(itemClassNotFound.getSimpleName(), itemIdNotFound);
    }

    /**
     * Helper constructor when not found item type could be class and finding was done by multiple params
     * @param itemClassNotFound class of not found item
     * @param params search params map
     */
    public NotFoundUserException(Class itemClassNotFound, Map<String, String> params) {
        super(
                ITEM_NOT_FOUND,
                "Item '" + itemClassNotFound.getSimpleName() + "' not found by params: " + params + "'"
        );
        this.getParametersMap().putAll(params);
        this.getParametersMap().put("type", itemClassNotFound.getSimpleName());
    }

}
