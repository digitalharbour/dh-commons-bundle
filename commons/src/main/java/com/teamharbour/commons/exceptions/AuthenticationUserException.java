package com.teamharbour.commons.exceptions;

import java.util.Map;

/**
 * Should be thrown when authentication error happened:
 * password not correct, signature check is fail, etc.
 * Usually, system knows which user is going to authenticate, so use {@link #AuthenticationUserException(String)} constructor.
 * E.g. in REST-API this error should be exposed as HTTP 401.
 *
 * Do not use this if there is some access-deny error (you KNOW the user is authenticated but he is not allowed for some action).
 * In this case use {@link NoAccessUserException}.
 *
 */
public class AuthenticationUserException extends CommonUserException {

    public AuthenticationUserException(String userNameFailsAuth) {
        super(CommonUserException.NOT_AUTHENTICATED, "User {} is not authenticated", "userName", userNameFailsAuth);
    }

    public AuthenticationUserException(String defaultMessagePattern, String... paramsAsOddKeyEvenValue) {
        super(CommonUserException.NOT_AUTHENTICATED, defaultMessagePattern, paramsAsOddKeyEvenValue);
    }

    public AuthenticationUserException(String defaultMessagePattern, Map<String, String> params) {
        super(CommonUserException.NOT_AUTHENTICATED, defaultMessagePattern, params);
    }
}
