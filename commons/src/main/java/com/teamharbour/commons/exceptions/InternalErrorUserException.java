package com.teamharbour.commons.exceptions;

import java.util.Map;

/**
 * An error occurred for reasons beyond the control of the user.
 * It normally napped to HTTP 500 errors.
 */
public class InternalErrorUserException extends CommonUserException {

    /**
     * Constructor using default "unknown_server_error" as message key
     */
    public InternalErrorUserException(String messagePattern, Map<String,String> params) {
        super(CommonUserException.UNKNOWN_SERVER_ERROR, messagePattern, params);
    }

    /**
     *  Constructor using default "unknown_server_error" as message key
     */
    public InternalErrorUserException(String message) {
        super(CommonUserException.UNKNOWN_SERVER_ERROR, message);
    }

    public InternalErrorUserException(String messageKey, String defaultMessage) {
        super(messageKey, defaultMessage);
    }

    public InternalErrorUserException(String messageKey, String defaultMessagePattern, Map<String, String> params) {
        super(messageKey, defaultMessagePattern, params);
    }

    public InternalErrorUserException(String messageKey, String defaultMessagePattern, String... paramsAsOddKeyEvenValue) {
        super(messageKey, defaultMessagePattern, paramsAsOddKeyEvenValue);
    }
}
