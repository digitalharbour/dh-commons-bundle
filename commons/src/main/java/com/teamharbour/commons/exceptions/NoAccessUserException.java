package com.teamharbour.commons.exceptions;

import java.util.Map;

/**
 * End User error delivered when classic 'no access' issue happens.
 * It is separate error for convenient handling as HTTP 403 or other controllers working with this issue
 */
public class NoAccessUserException extends CommonUserException {

    /**
     * Custom messages constructor
     */
    public NoAccessUserException(String defaultMessagePattern, String... params) {
        super(NOT_AUTHORIZED, defaultMessagePattern, params);
    }

    public NoAccessUserException(String defaultMessagePattern, Map<String, String> parameters) {
        super(NOT_AUTHORIZED, defaultMessagePattern, parameters);
    }
}