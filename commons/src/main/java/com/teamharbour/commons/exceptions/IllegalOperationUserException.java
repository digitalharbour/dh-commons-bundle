package com.teamharbour.commons.exceptions;

import java.util.Map;

/**
 * End User exception delivered when classic 'bad request' issue happens.
 * It is separate exception for convenient handling as HTTP 400 or other controllers working with this issue
 * Use when: request is correct (contains correct parameters) but can't be completed because of logical error
 * (1. Disable already disabled user, 2. Close already closed session and so on and so forth).
 * For other reasons like invalid input parameters,
 * preferable to use {@link ValidationUserException} or {@link BadParamUserException}
 */
public class IllegalOperationUserException extends CommonUserException {

    @Deprecated
    public IllegalOperationUserException(String... params) {
        super(ILLEGAL_OPERATION, "Illegal operation performed", params);
    }

    public IllegalOperationUserException(Map<String, String> params) {
        super(ILLEGAL_OPERATION, "Illegal operation performed", params);
    }

    public IllegalOperationUserException(String messageKey, String defaultMessage) {
        super(messageKey, defaultMessage);
    }

    public IllegalOperationUserException(String messageKey, String defaultMessagePattern, Map<String, String> parameters) {
        super(messageKey, defaultMessagePattern, parameters);
    }
}