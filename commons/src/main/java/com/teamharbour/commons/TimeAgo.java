package com.teamharbour.commons;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static java.lang.String.format;

/**
 * Time ago utility class
 * <p>
 * copied from: https://memorynotfound.com/calculate-relative-time-time-ago-java/
 * <p>
 * Shows elapsed time (milliseconds) or from start date to end date.
 * <p>
 * Examples of result output:
 * <p>
 * TimeAgo.toRelative(601000, 2)            ----- 10 minutes, 1 second ago
 * TimeAgo.toRelative(Long.MAX_VALUE, 1000) ----- 292471208 years, 8 months, 1 week, 7 hours, 12 minutes, 55 seconds ago
 * @deprecated use Instant.plus(), Instant.minus() and all the power of java.time package
 */
@Deprecated
public class TimeAgo {

    private TimeAgo() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * Times (year, month, week etc.)
     */
    public static final Map<String, Long> times = new LinkedHashMap<>();

    static {
        times.put("year", TimeUnit.DAYS.toMillis(365));
        times.put("month", TimeUnit.DAYS.toMillis(30));
        times.put("week", TimeUnit.DAYS.toMillis(7));
        times.put("day", TimeUnit.DAYS.toMillis(1));
        times.put("hour", TimeUnit.HOURS.toMillis(1));
        times.put("minute", TimeUnit.MINUTES.toMillis(1));
        times.put("second", TimeUnit.SECONDS.toMillis(1));
    }

    /**
     * Shows duration (milliseconds) as a string like this one:
     * 292471208 years, 8 months, 1 week, 7 hours, 12 minutes, 55 seconds ago
     *
     * @param duration milliseconds
     * @param maxLevel max level of times
     * @return elapsed time string
     */
    public static String toRelative(long duration, int maxLevel) {
        StringBuilder res = new StringBuilder();
        int level = 0;
        for (Map.Entry<String, Long> time : times.entrySet()) {
            long timeDelta = duration / time.getValue();
            if (timeDelta > 0) {
                res.append(timeDelta)
                        .append(" ")
                        .append(time.getKey())
                        .append(timeDelta > 1 ? "s" : "")
                        .append(", ");
                duration -= time.getValue() * timeDelta;
                level++;
            }
            if (level == maxLevel) {
                break;
            }
        }
        if ("".equals(res.toString())) {
            return "0 seconds ago";
        } else {
            res.setLength(res.length() - 2);
            res.append(" ago");
            return res.toString();
        }
    }

    /**
     * Shows duration (milliseconds) as a string like this one:
     * 292471208 years, 8 months, 1 week, 7 hours, 12 minutes, 55 seconds ago
     *
     * @param duration milliseconds
     * @return elapsed time string
     */
    public static String toRelative(long duration) {
        return toRelative(duration, times.size());
    }

    /**
     * Shows duration (between two dates) as a string like this one:
     * 292471208 years, 8 months, 1 week, 7 hours, 12 minutes, 55 seconds ago
     *
     * @param start start date
     * @param end   end date
     * @return elapsed time string
     */
    public static String toRelative(Date start, Date end) {
        if (start.after(end))
            throw new IllegalArgumentException(format("Start '%s' must be before end '%s'", start, end));
        return toRelative(end.getTime() - start.getTime());
    }

    /**
     * Shows duration (between two dates) as a string like this one:
     * 292471208 years, 8 months, 1 week, 7 hours, 12 minutes, 55 seconds ago
     *
     * @param start start date
     * @param end   end date
     * @param level max level of times
     * @return elapsed time string
     */
    public static String toRelative(Date start, Date end, int level) {
        if (start.after(end))
            throw new IllegalArgumentException(format("Start '%s' must be before end '%s'", start, end));
        return toRelative(end.getTime() - start.getTime(), level);
    }
}