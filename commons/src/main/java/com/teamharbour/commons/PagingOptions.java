package com.teamharbour.commons;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonView;
import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;

import static com.teamharbour.commons.PagingOptions.Type.OFFSET;
import static com.teamharbour.commons.PagingOptions.Type.PAGE;

@JsonInclude(JsonInclude.Include.NON_ABSENT)
@JsonView(ViewProfile.Basic.class)
// TODO: implement Class-level constraint (https://stackoverflow.com/questions/2781771/how-can-i-validate-two-or-more-fields-in-combination) to support paging
public class PagingOptions {

    private final Type type;

    @PositiveOrZero
    protected Long offset;

    @Positive
    @JsonView(ViewProfile.Minimal.class)
    protected Long page;

    @Positive
    @JsonView(ViewProfile.Minimal.class)
    protected Integer limit;

    private PagingOptions(int page, int itemsPerPage, Type type) {
        this((long) 0, itemsPerPage, type);
        this.updatePage(page);
    }

    private PagingOptions(long offset, int limit, Type type) {
        if (offset < 0)
            throw new IllegalArgumentException("Offset must be 0 or more");
        if (limit < 0)
            throw new IllegalArgumentException("Limit must be 0 or more");
        this.offset = offset;
        this.limit = limit;
        this.type = type;
    }

    /**
     * Creates paging options using just offset and limit (free pagination).
     *
     * @param offset offset: zero means 1st element
     * @param limit max items will be returned
     * @return paging options from offset and limit
     */
    public static PagingOptions createOffsetAndLimit(long offset, int limit) {
        return new PagingOptions(offset, limit, OFFSET);
    }

    /**
     * Creates classic paging options by page and items per page
     *
     * @param page page: first page
     * @param limit limit of items
     */
    public static PagingOptions createPageAndLimit(int page, int limit) {
        return new PagingOptions(page, limit, PAGE);
    }

    public long getOffset() {
        return offset;
    }

    @Schema(description = "max items per page")
    public int getLimit() {
        return limit;
    }

    @Schema(description = "page number, starting from `1`")
    public long getPage() {
        if (this.isPageType())
            return this.page;
        else
            return 1L + (long) this.offset / this.limit;
    }

    public void updatePage(long newCurrentPage) {
        if (newCurrentPage < 1)
            throw new IllegalArgumentException("Page must be 1 or more");
        this.page = newCurrentPage;
        this.offset = this.limit * (this.page - 1);
    }

    @JsonIgnore
    public boolean isOffsetType() {
        return type == OFFSET;
    }

    @JsonIgnore
    public boolean isPageType() {
        return type == PAGE;
    }

    enum Type {
        OFFSET, PAGE
    }

    @Override
    public String toString() {
        return "PagingOptions{" +
                (
                        OFFSET.equals(this.type) ? ("offset=" + offset) : ("page=" + page)
                ) +
                ", limit=" + limit +
                '}';
    }
}
