package com.teamharbour.commons;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import static java.lang.System.lineSeparator;
import static java.util.stream.Collectors.joining;

public class FileUtils {

    private static Logger logger = LoggerFactory.getLogger(FileUtils.class);

    private FileUtils() {
        throw new IllegalStateException("Utils class");
    }

    /**
     * Reads given resource file as a string.
     *
     * @param fileName the path to the resource file
     * @return the file's contents or null if the file could not be opened
     */
    public static String getResourceFileAsString(String fileName) {
        logger.info("Reading [{}] from resources", fileName);

        ClassLoader classLoader = ClassLoader.getSystemClassLoader();

        try (InputStream inputStream = classLoader.getResourceAsStream(fileName)) {
            if (inputStream != null) {
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                String result = reader.lines().collect(joining(lineSeparator()));
                logger.trace("File has been read as string");
                return result;
            }

            throw new IllegalArgumentException("Input stream is null");
        } catch (IOException e) {
            throw new IllegalArgumentException("Failed reading [" + fileName + "] from resources", e);
        }
    }
}