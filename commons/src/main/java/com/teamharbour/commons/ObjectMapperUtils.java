package com.teamharbour.commons;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.*;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @deprecated do not use this, it does nothing but wraps checked exceptions into runtime (checked exceptions for old fags, huh?)
 */
@Deprecated
public class ObjectMapperUtils {

    private ObjectMapperUtils() {
        throw new IllegalStateException("Utility class");
    }

    // ----------------------------- SERIALIZE -------------------------------------------------------------------------

    public static <T> String serializeLinkedHashSet(Set<T> linkedHashSet) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            return objectMapper.writeValueAsString(checkNotNull(linkedHashSet));
        } catch (JsonProcessingException e) {
            throw new IllegalArgumentException("Can't serialize as LinkedHashSet", e);
        }
    }

    public static <T> String serializeSet(Set<T> set) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            return objectMapper.writeValueAsString(checkNotNull(set));
        } catch (JsonProcessingException e) {
            throw new IllegalArgumentException("Can't serialize as Set", e);
        }
    }

    public static <T> String serializeList(List<T> list) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            return objectMapper.writeValueAsString(checkNotNull(list));
        } catch (JsonProcessingException e) {
            throw new IllegalArgumentException("Can't serialize List", e);
        }
    }

    public static String serialize(Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            throw new IllegalArgumentException("Unable to serialize " + obj, e);
        }
    }

    public static Map<String, Object> serializeAsMap(Object obj) {
        return new ObjectMapper().convertValue(obj, Map.class);
    }

    // --------------------------- DESERIALIZE -------------------------------------------------------------------------

    public static <T> T deserialize(String input, Class<T> clazz) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            return objectMapper.readValue(input, objectMapper.getTypeFactory().constructType(checkNotNull(clazz)));
        } catch (IOException e) {
            throw new IllegalArgumentException("Can't deserialize as " + clazz.getName(), e);
        }
    }

    public static <T> T deserialize(Object input, Class<T> clazz) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            String serialized = serialize(input);
            return objectMapper.readValue(serialized, objectMapper.getTypeFactory().constructType(checkNotNull(clazz)));
        } catch (IOException e) {
            throw new IllegalArgumentException("Can't deserialize as " + clazz.getName(), e);
        }
    }

    public static <T> List<T> deserializeAsList(String input, Class<T> clazz) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            return objectMapper.readValue(input, objectMapper.getTypeFactory().constructCollectionType(List.class, checkNotNull(clazz)));
        } catch (IOException e) {
            throw new IllegalArgumentException("Can't deserialize as List of " + clazz.getName(), e);
        }
    }

    public static <T> Set<T> deserializeAsSet(String input, Class<T> clazz) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            return objectMapper.readValue(input, objectMapper.getTypeFactory().constructCollectionType(Set.class, checkNotNull(clazz)));
        } catch (IOException e) {
            throw new IllegalArgumentException("Can't deserialize as Set of " + clazz.getName(), e);
        }
    }

    public static <T> Set<T> deserializeAsLinkedHashSet(String input, Class<T> clazz) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            return objectMapper.readValue(input, objectMapper.getTypeFactory().constructCollectionType(LinkedHashSet.class, checkNotNull(clazz)));
        } catch (IOException e) {
            throw new IllegalArgumentException("Can't deserialize as LinkedHashSet of " + clazz.getName(), e);
        }
    }

    public static Map<String, String> deserializeAsStringMap(String input) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            return objectMapper.readValue(checkNotNull(input), objectMapper.getTypeFactory().constructMapType(Map.class, String.class, String.class));
        } catch (IOException e) {
            throw new IllegalArgumentException("Can't deserialize as string Map", e);
        }
    }

    public static Map<String, Object> deserializeAsObjectMap(String input) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            return objectMapper.readValue(checkNotNull(input), objectMapper.getTypeFactory().constructMapType(Map.class, String.class, Object.class));
        } catch (IOException e) {
            throw new IllegalArgumentException("Can't deserialize as object Map", e);
        }
    }

    /**
     * Returns list of map(String, String) values
     *
     * @param input array of json
     * @return list of map
     */
    public static List<Map<String, String>> deserializeUnknownJsonList(String input) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            TypeReference<List<Map<String, String>>> typeRef = new TypeReference<>() {
            };
            return objectMapper.readValue(checkNotNull(input), typeRef);
        } catch (IOException e) {
            throw new IllegalArgumentException("Can't deserialize as string Map", e);
        }
    }

    public static Map<String, String> deserializeAsStringLinkedHashMap(String input) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            return objectMapper.readValue(checkNotNull(input), objectMapper.getTypeFactory().constructMapType(LinkedHashMap.class, String.class, String.class));
        } catch (IOException e) {
            throw new IllegalArgumentException("Can't deserialize as string LinkedHashMap", e);
        }
    }
}