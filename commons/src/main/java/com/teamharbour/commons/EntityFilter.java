package com.teamharbour.commons;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.lang.String.format;

/**
 * Filter settings for finding some entities.
 * Due it is CommonForm, it is possible to add any attributes for quick business logic extension
 * (e.g. searching by some very specific attribute).
 * Allows just "find all" flag, contains optional query string and paging options.
 * Supports filtering: by query string, by createdFrom..createdTo and by createdLastDays criteria.
 *
 * @deprecated use PagingForm, SortingForm, PeriodFilter and build own custom XxxFilterFrom class combining previous
 */
@Deprecated
public class EntityFilter extends CommonForm {

    /**
     * All-filter limit (1000 items defined to prevent overheads)
     */
    public static final int ALL_LIMIT = 1000;

    /**
     * All-filter. Use it carefully if you sure there are impossible to obtain too many items.
     * However ALL_LIMIT will be applied anyway
     */
    public static final EntityFilter ALL = new EntityFilter(
            true,
            PagingOptions.createOffsetAndLimit(0, ALL_LIMIT)
    );
    public static final String ATTR_CREATED_FROM = "createdFrom";
    public static final String ATTR_CREATED_TO = "createdTo";
    public static final String ATTR_UPDATED_FROM = "updatedFrom";
    public static final String ATTR_UPDATED_TO = "updatedTo";

    private boolean isFindAllFilter;

    private String query;

    private PagingOptions pagingOptions;


    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Instant createdFrom;

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Instant createdTo;

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Instant updatedFrom;

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Instant updatedTo;

    private Integer createdLastDays;

    private Integer updatedLastDays;

    private ZoneOffset timeZone = ZoneOffset.UTC;

    /**
     * Default constructor for easy instantiation (from same package).
     */
    public EntityFilter() {
        this.isFindAllFilter = false;
        this.query = "";
        this.pagingOptions = PagingOptions.createOffsetAndLimit(0, ALL_LIMIT);
    }

    protected EntityFilter(boolean isFindAllFilter, PagingOptions pagingOptions) {
        this.isFindAllFilter = isFindAllFilter;
        this.pagingOptions = checkNotNull(pagingOptions);
        this.query = "";
    }

    public void setQuery(String query) {
        this.query = checkNotNull(query);
    }

    public void setPagingOptions(PagingOptions pagingOptions) {
        this.pagingOptions = checkNotNull(pagingOptions);
    }

    public String getQuery() {
        return this.query;
    }

    public boolean isFindAllFilter() {
        return isFindAllFilter;
    }

    public PagingOptions getPagingOptions() {
        return pagingOptions;
    }

    public Instant getCreatedFrom() {
        return createdFrom;
    }

    public void setCreatedFrom(Instant createdFrom) {
        this.createdFrom = createdFrom;
    }

    public void setUpdatedTo(Instant updatedTo) {
        this.updatedTo = updatedTo;
    }

    public void setUpdatedFrom(Instant updatedFrom) {
        this.updatedFrom = updatedFrom;
    }

    public Instant getCreatedTo() {
        return createdTo;
    }

    public Instant getUpdatedTo() {
        return updatedTo;
    }

    public Instant getUpdatedFrom() {
        return updatedFrom;
    }

    public void setCreatedTo(Instant createdTo) {
        this.createdTo = createdTo;
    }

    public void setCreatedLastDays(Integer createdLastDays) {
        this.createdLastDays = createdLastDays;
    }

    public void setUpdatedLastDays(Integer updatedLastDays) {
        this.updatedLastDays = updatedLastDays;
    }

    public Optional<ZoneId> getTimeZone() {
        return Optional.ofNullable(timeZone);
    }

    public void setTimeZone(ZoneOffset timeZone) {
        this.timeZone = timeZone;
    }

    public boolean hasCreatedRestriction() {
        return (this.getCreatedFrom() != null && this.getCreatedTo() != null);
    }

    public boolean acceptedByCreatedRestriction(AbstractEntity entity) {

        if (!this.hasCreatedRestriction())
            return true;

        return entity.getCreatedAt().isAfter(this.createdFrom) &&
                entity.getCreatedAt().isBefore(this.createdTo);
    }

    @Override
    public void putAttribute(String key, String value) {
        super.putAttribute(key, value);

        if ("timeZone".equals(key)) {
            this.timeZone = ZoneId.of(value).getRules().getOffset(Instant.now());
            this.updateDatetimeFieldsForZone();
        }

        if (ATTR_CREATED_FROM.equals(key)) this.setCreatedFrom(CommonsUtils.parseAsDateOrDateTime(this.timeZone, value));
        if (ATTR_CREATED_TO.equals(key)) this.setCreatedTo(CommonsUtils.parseAsDateOrDateTime(this.timeZone, value, true));
        if (ATTR_UPDATED_FROM.equals(key)) this.setUpdatedFrom(CommonsUtils.parseAsDateOrDateTime(this.timeZone, value));
        if (ATTR_UPDATED_TO.equals(key)) this.setUpdatedTo(CommonsUtils.parseAsDateOrDateTime(this.timeZone, value, true));
    }

    private void updateDatetimeFieldsForZone() {
        if (this.getAttributes().containsKey(ATTR_CREATED_FROM))
            this.putAttribute(ATTR_CREATED_FROM, this.getAttributes().get(ATTR_CREATED_FROM));
        if (this.getAttributes().containsKey(ATTR_CREATED_TO))
            this.putAttribute(ATTR_CREATED_TO, this.getAttributes().get(ATTR_CREATED_TO));
        if (this.getAttributes().containsKey(ATTR_UPDATED_FROM))
            this.putAttribute(ATTR_UPDATED_FROM, this.getAttributes().get(ATTR_UPDATED_FROM));
        if (this.getAttributes().containsKey(ATTR_UPDATED_TO))
            this.putAttribute(ATTR_UPDATED_TO, this.getAttributes().get(ATTR_UPDATED_TO));
    }

    @Override
    public String toString() {
        String attrs = getAttributes()
                .entrySet()
                .stream()
                .map(e -> format("[key=%s, val=%s]", e.getKey(), e.getValue()))
                .collect(Collectors.joining(", "));

        return "EntityFilter{" +
                "isFindAllFilter=" + isFindAllFilter +
                ", query='" + query + '\'' +
                ", pagingOptions=" + pagingOptions +
                ", createdFrom=" + createdFrom +
                ", createdTo=" + createdTo +
                ", updatedFrom=" + updatedFrom +
                ", updatedTo=" + updatedTo +
                ", createdLastDays=" + createdLastDays +
                ", updatedLastDays=" + updatedLastDays +
                ", timeZone=" + timeZone +
                ", attributes=" + attrs +
                '}';
    }
}
