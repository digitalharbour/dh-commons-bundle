package com.teamharbour.commons;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonView;
import io.swagger.v3.oas.annotations.media.Schema;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.lang.String.format;

/**
 * List of objects, which means there is part of paging navigation or offset-limit slice.
 *
 * @param <T> type of list items
 */
@JsonPropertyOrder({"totalCount", "pagingOptions", "hasPrevious", "hasNext", "count", "list"})
@JsonView(ViewProfile.Basic.class)
public class PagingList<T> {

    @JsonView(ViewProfile.Minimal.class)
    private final Long totalCount;

    @JsonView(ViewProfile.Minimal.class)
    private final PagingOptions pagingOptions;

    private final boolean hasPrevious;

    private final boolean hasNext;

    private final Integer count;

    @JsonView(ViewProfile.Minimal.class)
    private final ArrayList<T> list = new ArrayList<>();

    public PagingList(Collection<T> items, PagingOptions pagingOptions, long totalCount) {

        // check if items not longer than limit
        if (checkNotNull(items).size() > pagingOptions.getLimit())
            throw new IllegalArgumentException(format("Items length %s is bigger than paging options limit %s", items.size(), pagingOptions.getLimit()));

        this.list.addAll(items);

        this.count = this.list.size();

        this.pagingOptions = pagingOptions;

        this.totalCount = totalCount;

        this.hasPrevious = pagingOptions.getOffset() > 0;

        this.hasNext = pagingOptions.getOffset() + pagingOptions.getLimit() < this.totalCount;
    }

    @Schema(description = "list of items", required = true)
    public List<T> getList() {
        return list;
    }

    @Schema(description = "`true` if next page contains any items")
    public boolean isHasNext() {
        return hasNext;
    }

    @Schema(description = "`true` if previous page contains any items")
    public boolean isHasPrevious() {
        return hasPrevious;
    }

    @Schema(description = "total items count on all pages")
    public Long getTotalCount() {
        return totalCount;
    }

    @Schema(description = "paging options which was provided in request")
    public PagingOptions getPagingOptions() {
        return this.pagingOptions;
    }

    @Schema(description = "items count of current page; always equal to `list` length")
    public Integer getCount() {
        return count;
    }
}
