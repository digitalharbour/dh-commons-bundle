package com.teamharbour.commons;

import org.apache.commons.beanutils.PropertyUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.function.BiConsumer;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.lang.Integer.valueOf;
import static java.lang.String.format;
import static java.util.Optional.ofNullable;

public class CommonsUtils {

    private static final String DEFAULT_GIT_PROPERTIES_RESOURCE_NAME = "/git.properties";

    private static Logger logger = LoggerFactory.getLogger(CommonsUtils.class);

    // How many bytes in one megabyte
    private static final int BYTES_IN_MEGABYTE = 1048576;

    // How many bytes in one kilobyte
    private static final int BYTES_IN_KILOBYTE = 1024;

    // Prevents class instantiation
    private CommonsUtils() {
    }

    /**
     * Masks string to non-disclose (for secret strings printing to log or for other debug reasons).
     * Accept null origin (returns "null").
     *
     * @param origin     string to mask
     * @param firstChars how many first chars may be disclosed
     * @param lastChars  how many last chars may be disclosed
     * @return masked string like "int....tion" for "internationalization" with firstChars=3, lastChars=4
     */
    public static String maskString(CharSequence origin, int firstChars, int lastChars) {

        if (origin == null) return "null";

        // if string too short for masking, just first and last symbols
        int len = origin.length();
        if (len <= firstChars + lastChars) {
            return origin.toString();
        }
        return origin.subSequence(0, firstChars) + "..." + origin.subSequence(len - lastChars, len);
    }

    /**
     * Converts Mapn&lt;String, String&gt; to array of map's values in
     *
     * @param map map
     * @return array of values
     */
    public static String[] mapToValuesArray(Map<String, String> map) {
        ArrayList<String> res = new ArrayList<>(checkNotNull(map).size());
        res.addAll(map.values());

        return res.toArray(new String[0]);
    }

    /**
     * Converts Map&lt;String, String&gt; to list of map's values
     *
     * @param map map
     * @return list of values
     */
    public static List<String> mapToValuesList(Map<String, String> map) {
        return Arrays.asList(CommonsUtils.mapToValuesArray(checkNotNull(map)));
    }

    /**
     * Converts "name1", "value1", "name2", "value2" to Map{name1=&gt;value1, name2=&gt;value2}.
     * Throws {@link IllegalArgumentException} if not even quantity
     *
     * @param nameValuePairs name and value pairs
     * @return map name=&gt;value
     */
    public static Map<String, String> arrayToMap(String... nameValuePairs) {

        if (nameValuePairs.length % 2 == 1)
            throw new IllegalArgumentException();

        Map<String, String> map = new LinkedHashMap<>();
        boolean isName = true;
        String name = null;
        for (String x : nameValuePairs) {
            if (isName) name = x;
            else map.put(name, x);
            isName = !isName;
        }

        return map;
    }


    /**
     *
     * @param pattern string patter like "Some param '{}' has value: {}"
     * @param values values for replacing '{}'
     * @return formatted message
     */
    public static String formatMessage(String pattern, String... values) {
        String res = checkNotNull(pattern);
        for (String value : values) {
            res = res.replaceFirst("\\{}", ofNullable(value).orElse("null"));
        }
        return res;
    }

    /**
     * Get code version from META-INF
     * Works as jar version, see: https://stackoverflow.com/questions/37642837/gradle-make-build-version-available-to-java
     *
     * @return code version
     */
    public static String getCodeVersion() {
        return CommonsUtils.class.getPackage().getImplementationVersion();
    }

    /**
     * @param value string date OR datetime value like "2017-01-01" or "2017-01-01T15:59..."
     * @return parsed Instant object
     */
    public static Instant parseAsDateOrDateTime(ZoneId zone, String value) {
        return parseAsDateOrDateTime(zone, value, false);
    }

    /**
     * @param value                string date OR datetime value like "2017-01-01" or "2017-01-01T15:59..."
     * @param rollToEndOfDayIfDate if it is date (without time), roll time to end of day (23:59:59.999), elsewhere 00:00:00.000
     * @return parsed Instant object
     */
    public static Instant parseAsDateOrDateTime(ZoneId zone, String value, boolean rollToEndOfDayIfDate) {
        LocalDateTime dateTime;
        if (checkNotNull(value).length() > 10) {
            dateTime = LocalDateTime.parse(value);
        } else {
            LocalDate date = LocalDate.parse(value);
            dateTime = rollToEndOfDayIfDate ? date.atTime(23, 59, 59, 999999999) : date.atStartOfDay();
        }
        return dateTime.toInstant(checkNotNull(zone).getRules().getOffset(dateTime));
    }

    public static int convertToLengthInBytes(String quota) {
        String[] chunks = quota.split(" ");
        if (chunks.length != 2) {
            throw new IllegalStateException("Quota is malformed. Must look like that (for example): 12 MB");
        }

        int value;
        try {
            value = Integer.parseInt(chunks[0]);
        } catch (NumberFormatException e) {
            throw new IllegalStateException("Quota is malformed. Value must be integer. For example: 12 MB");
        }

        String unit = chunks[1].toUpperCase();
        // @formatter:off
        switch (unit) {
            case "B": return value;
            case "KB": return value * BYTES_IN_KILOBYTE;
            case "MB": return value * BYTES_IN_MEGABYTE;
            default:
                throw new IllegalStateException(format("Quota is malformed. Unit '%s' not supported", unit));
        }
        // @formatter:on
    }

    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    public static String optToStr(Optional opt) {
        if (opt.isPresent()) {
            return opt.get().toString();
        } else {
            return "[empty]";
        }
    }

    /**
     * Returns copy of values if not empty or single-item collection
     * containing neverHappenedValue if values was empty.
     * It helps to avoid errors in SQL "where field in ()"
     * @param values collection of values for "SQL IN (...)"
     * @param neverHappenedValue value that never can happened in data
     * @param <T> any Object type of values
     * @return copy of values or (neverHappenedValue) collection
     */
    public static <T> Collection<T> safeSqlInValues(Collection<T> values, T neverHappenedValue) {
        if (values.isEmpty())
            return Collections.singletonList(neverHappenedValue);
        else
            return new ArrayList<>(values);
    }

    /**
     * Loads git-properties from resource.
     *
     * @param clazz        class which use for resource load
     * @param resourceName resource name
     * @return git information
     * @throws IllegalStateException if resource not found or IO-error on loading
     * @see GitInfo#GitInfo(Properties)
     */
    public static GitInfo loadGitInfo(Class clazz, String resourceName) {

        Properties gitProps = new Properties();

        try (InputStream in = clazz.getResourceAsStream(resourceName)) {
            if (in == null)
                throw new IllegalStateException(format("Cannot find resource %s for class %s", resourceName, clazz));
            gitProps.load(in);
        } catch (IOException e) {
            throw new IllegalStateException(format("Cannot load resource %s for class %s", resourceName, clazz), e);
        }

        return new GitInfo(gitProps);
    }

    /**
     * @return git info object built from "/git.properties"
     * @throws IllegalStateException if "/git.properties" resource not found in root of classpath
     * @see GitInfo#GitInfo(Properties)
     * @see CommonsUtils#loadGitInfo(Class, String)
     */
    public static GitInfo loadGitInfo() {
        return CommonsUtils.loadGitInfo(CommonsUtils.class, DEFAULT_GIT_PROPERTIES_RESOURCE_NAME);
    }

    /**
     * Copy properties from one bean to another.
     *
     * @param source            source bean
     * @param destination       destination bean
     * @param additionalHandler handler for additional handling
     *                          (when some bean's property must be converted somehow instead of simple copying)
     * @param propsToCopy       array of property names to be copied
     */
    public static void copyProps(Object source, Object destination, BiConsumer<Object, Object> additionalHandler, String... propsToCopy) {

        for (String propToCopy : propsToCopy) {

            // ignore whitespaces
            propToCopy = propToCopy.trim();

            String sourceProperty;
            String destinationProperty;

            if (propToCopy.contains("->")) {
                String[] split = propToCopy.split("->");
                sourceProperty = split[0];
                destinationProperty = split[1];
            } else {
                sourceProperty = propToCopy;
                destinationProperty = propToCopy;
            }

            try {
                Object name = PropertyUtils.getSimpleProperty(source, sourceProperty);
                PropertyUtils.setSimpleProperty(destination, destinationProperty, name);
            } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
                logger.error("Failed copy source property {} to destination property {}. Source bean {}, destination bean {}", sourceProperty, destinationProperty, source, destination, e);
            }
        }

        logger.trace("Main properties copied");

        if (additionalHandler != null) {
            logger.trace("Invoking additional Handler");
            additionalHandler.accept(source, destination);
        }
    }
}