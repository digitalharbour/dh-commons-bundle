package com.teamharbour.commons;


import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.Pattern;

@Schema(title = "Sorting parameters")
public class SortingForm {

    @Pattern(regexp = "^[a-zA-Z_$][a-zA-Z_$0-9]{0,100}$")
    private String sortBy;

    @Pattern(regexp = "^(asc|desc)$")
    private String sortOrder = "asc";

    public SortingForm(@Pattern(regexp = "^[a-zA-Z_$][a-zA-Z_$0-9]{0,100}$") String sortBy, @Pattern(regexp = "^(asc|desc)$") String sortOrder) {
        this.sortBy = sortBy;
        this.sortOrder = sortOrder;
    }

    public SortingForm() {
    }

    @Schema(description = "by which attribute to sort", required = false)
    public String getSortBy() {
        return sortBy;
    }

    public void setSortBy(String sortBy) {
        this.sortBy = sortBy;
    }

    @Schema(description = "sort direction: 'asc' or 'desc' are allowed", required = false)
    public String getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(String sortOrder) {
        this.sortOrder = sortOrder;
    }

    @Schema(hidden = true)
    public boolean isDefined() {
        return this.sortBy != null;
    }

    @Override
    public String toString() {
        return "SortingForm{" +
                (this.isDefined() ? "by '" + this.sortBy + "' " + this.sortOrder : "default") +
                '}';
    }
}
