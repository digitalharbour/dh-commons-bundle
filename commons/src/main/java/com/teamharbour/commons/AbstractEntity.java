package com.teamharbour.commons;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.v3.oas.annotations.media.Schema;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.Instant;

@JsonInclude(JsonInclude.Include.NON_ABSENT)
@MappedSuperclass
public class AbstractEntity {
    /**
     * Entity ID. Must be allowed to access from children for specific cases
     * (e.g. importing entities from outside, framework requirements to have public setter, etc.)
     */
    @Id
    @NotNull @NotBlank
    protected String id;

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @Column(name = "created_at", nullable = false, columnDefinition = "TIMESTAMP")
    @Convert(converter = JPAInstantConverter.class)
    @NotNull
    private Instant createdAt;


    public AbstractEntity(String id, Instant createdAt) {
        this.id = id;
        this.createdAt = createdAt;
    }

    @Schema(description = "unique entity ID", required = true)
    public String getId() {
        return id;
    }

    @Schema(description = "created timestamp", required = true)
    public Instant getCreatedAt() {
        return createdAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AbstractEntity that = (AbstractEntity) o;

        return this.getId().equals(that.getId());
    }

    @Override
    public int hashCode() {
        return this.getId().hashCode();
    }
}
