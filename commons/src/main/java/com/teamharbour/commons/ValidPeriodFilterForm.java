package com.teamharbour.commons;


import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE })
@Retention(RUNTIME)
@Constraint(validatedBy = PeriodFilterFormValidator.class)
public @interface ValidPeriodFilterForm {

    String message() default "{invalidPeriodFilter}";

    Class<?>[] groups() default { };

    Class<? extends Payload>[] payload() default {};

}
