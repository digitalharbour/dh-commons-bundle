package com.teamharbour.commons;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Environment context should be provided for sessions from controllers.
 * EnvironmentContext should give full information about user request,
 * environment variables, any specific system info.
 * Context must define essential user data: who, from where, by what agent.
 * E.g. for CLI it could be: actingUserName=username, userAddress=console ID or "local" string, userAgent=shell name;
 * For HTTP user it could be: actingUserName=username, userAddress=IP address, userAgent=HTTP client name;
 */
@JsonInclude(JsonInclude.Include.NON_ABSENT)
public abstract class EnvironmentContext implements Serializable {

    private HashMap<String, String> attributes = new HashMap<>();


    public void setAttribute(String name, String value) {
        if (value == null) throw new IllegalArgumentException("Attribute " + name + " can not be null");

        this.attributes.put(name, value);
    }

    public boolean hasAttribute(String name) {
        return this.attributes.get(name) != null;
    }

    public String getAttribute(String name) {
        if (this.hasAttribute(name))
            return this.attributes.get(name);
        throw new IllegalArgumentException("Attribute '" + name + "' not exists");
    }

    @JsonIgnore
    public Collection<String> getAttributeNames() {
        return this.attributes.keySet();
    }

    public Map<String,String> getAttributes() {
        return this.attributes;
    }

    public abstract String getUserName();

    public abstract String getUserAddress();

    public abstract String getUserAgent();
}
