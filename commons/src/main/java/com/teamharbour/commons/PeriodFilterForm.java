package com.teamharbour.commons;

import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.NegativeOrZero;
import javax.validation.constraints.Positive;
import java.time.*;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@ValidPeriodFilterForm
public class PeriodFilterForm {

    static final String FROM_AND_TO = "fromAndTo";
    static final String ONE_DAY = "oneDay";
    static final String ONE_WEEK = "oneWeek";
    static final String ONE_MONTH = "oneMonth";
    static final String LAST_DAYS = "lastDays";
    static final String LAST_WEEKS = "lastWeeks";
    static final String LAST_MONTHS = "lastMonths";
    public static final String ONE_QUARTER = "oneQuarter";
    public static final String LAST_QUARTERS = "lastQuarters";

    private final Map<String, Strategy> strategyMap = new HashMap<>();
    private EffectivePeriod effectivePeriod;
    private Clock clock;
    private LocalDate from;
    private LocalDate to;

    private ZoneId zone = ZoneId.of("UTC");

    @NegativeOrZero
    private Integer oneDay;
    @NegativeOrZero
    private Integer oneWeek;
    @NegativeOrZero
    private Integer oneMonth;
    @NegativeOrZero
    private Integer oneQuarter;
    @Positive
    private Integer lastDays;
    @Positive
    private Integer lastWeeks;
    @Positive
    private Integer lastMonths;
    @Positive
    private Integer lastQuarters;

    /**
     * Initializes strategies map
     */
    public PeriodFilterForm() {

        this.clock = Clock.systemUTC(); // default clock, could be replaced for testing, see #withClock()

        this.strategyMap.put(FROM_AND_TO, f -> new EffectivePeriod(f.from, f.to, f.zone));

        this.strategyMap.put(ONE_DAY, f -> {
            LocalDate base = f.getNowDate().plusDays(f.oneDay);
            return new EffectivePeriod(base, base, f.zone);
        });

        this.strategyMap.put(ONE_WEEK, f -> {
            LocalDate base = f.getNowDate().plusWeeks(f.oneWeek);
            return new EffectivePeriod(
                    base.with(TemporalAdjusters.previous(DayOfWeek.MONDAY)),
                    base.with(TemporalAdjusters.next(DayOfWeek.SUNDAY) ),
                    f.zone
            );
        });

        this.strategyMap.put(ONE_MONTH, f -> {
            LocalDate base = f.getNowDate().plusMonths(f.oneMonth);
            return new EffectivePeriod(base.with(TemporalAdjusters.firstDayOfMonth()), base.with(TemporalAdjusters.lastDayOfMonth()), f.zone);
        });

        this.strategyMap.put(LAST_DAYS, f -> {
            LocalDate base = f.getNowDate().minusDays(f.lastDays-1L);
            return new EffectivePeriod(base, f.getNowDate(), f.zone);
        });

        this.strategyMap.put(LAST_WEEKS, f -> {
            LocalDate base = f.getNowDate().minusWeeks(f.lastWeeks-1L);
            return new EffectivePeriod(base.with(TemporalAdjusters.previous(DayOfWeek.MONDAY)), f.getNowDate(), f.zone);
        });

        this.strategyMap.put(LAST_MONTHS, f -> {
            LocalDate base = f.getNowDate().minusMonths(f.lastMonths-1L);
            return new EffectivePeriod(base.with(TemporalAdjusters.firstDayOfMonth()), f.getNowDate(), f.zone);
        });

    }

    PeriodFilterForm withClock(Clock clock) {
        this.clock = clock;
        return this;
    }

    LocalDate getNowDate() {
        return LocalDate.now(this.clock.withZone(this.zone));
    }

    @Schema(description = "date from which selection should be done; always means 00:00:00 time; if there is 'from', 'to' must be defined and vice-versa", required = false, example = "EST")
    public LocalDate getFrom() {
        return this.from;
    }

    @Schema(description = "date to which selection should be done; always means 23:59:59 if there is 'from', 'to' must be defined and vice-versa", required = false, example = "EST")
    public LocalDate getTo() {
        return this.to;
    }

    @Schema(description = "which particular day should be taken as period from 00:00:00 till 23:59:59; '0' means today, '-1' means yesterday, etc.", required = false)
    public Integer getOneDay() {
        return oneDay;
    }

    @Schema(description = "which particular week should be taken as period from Mon 00:00:00 till Sun 23:59:59; '0' means current week, '-1' means last week, etc.", required = false)
    public Integer getOneWeek() {
        return oneWeek;
    }

    @Schema(description = "which particular month should be taken as period from 1st till last day of month 23:59:59; '0' means current month, '-1' means last month, etc.", required = false)
    public Integer getOneMonth() {
        return oneMonth;
    }

    @Schema(description = "which particular quarter should be taken as period from 1st day at 00:00:00 till last day of quarter at 23:59:59; '0' means current quarter, '-1' means last quarter, etc.", required = false)
    public Integer getOneQuarter() {
        return oneQuarter;
    }

    @Schema(description = "how many days from today should be taken as period from 00:00 of past day till today at 23:59:59; '1' means from 00:00 today, '2' from 00:00 yesterday, etc.", required = false)
    public Integer getLastDays() {
        return lastDays;
    }

    @Schema(description = "how many weeks from now should be taken as period from Mon 00:00 till today at 23:59:59; '1' means from current week Monday, '2' from Monday of week before current, etc.", required = false)
    public Integer getLastWeeks() {
        return lastWeeks;
    }

    @Schema(description = "how many months from now should be taken as period from 1st 00:00 till today at 23:59:59; '1' means 1st day of current month, '2' 1st of previous month, etc.", required = false)
    public Integer getLastMonths() {
        return lastMonths;
    }

    @Schema(description = "how many quarters from now should be taken as period from 1st day of quarter at 00:00:00 till today at 23:59:59; '1' means 1st day of current quarter, '2' means 1st of previous quarter, etc.", required = false)
    public Integer getLastQuarters() {
        return lastQuarters;
    }

    @Schema(example = "+02:00, UTC, Europe/Berlin")
    public String getZone() {
        return zone.getId();
    }

    public void setFrom(String from) {
        this.from = LocalDate.parse(from);
    }

    public void setTo(String to) {
        this.to = LocalDate.parse(to);
    }

    public void setZone(String zone) {
        this.zone = ZoneId.of(zone, ZoneId.SHORT_IDS);
    }

    public void setOneDay(Integer oneDay) {
        this.oneDay = oneDay;
    }

    public void setOneWeek(Integer oneWeek) {
        this.oneWeek = oneWeek;
    }

    public void setOneMonth(Integer oneMonth) {
        this.oneMonth = oneMonth;
    }

    public void setOneQuarter(Integer oneQuarter) {
        this.oneQuarter = oneQuarter;
    }

    public void setLastDays(Integer lastDays) {
        this.lastDays = lastDays;
    }

    public void setLastWeeks(Integer lastWeeks) {
        this.lastWeeks = lastWeeks;
    }

    public void setLastMonths(Integer lastMonths) {
        this.lastMonths = lastMonths;
    }

    public void setLastQuarters(Integer lastQuarters) {
        this.lastQuarters = lastQuarters;
    }

    @Schema(hidden = true) // strange but here it is working ok
    public LocalDate getEffectiveFromDate() {
        if (this.effectivePeriod == null) {
            this.buildEffectivePeriod();
        }
        return this.effectivePeriod.from;
    }

    @Schema(hidden = true) // strange but here it is working ok
    public LocalDate getEffectiveToDate() {
        if (this.effectivePeriod == null) {
            this.buildEffectivePeriod();
        }
        return this.effectivePeriod.to;
    }

    /**
     * @return real ("effective") timestamp of period start
     * @throws IllegalStateException if no period strategies found, use {@link #isDefined()} to check
     */
    @Schema(hidden = true) // strange but here it is working ok
    public Instant getEffectiveFrom() {

        if (this.effectivePeriod == null) {
            this.buildEffectivePeriod();
        }

        return this.effectivePeriod.getEffectiveFrom();
    }

    /**
     * @return real ("effective") timestamp of period end
     * @throws IllegalStateException if no period strategies found, use {@link #isDefined()} to check
     */
    @Schema(hidden = true)
    public Instant getEffectiveTo() {
        if (this.effectivePeriod == null) {
            this.buildEffectivePeriod();
        }

        return this.effectivePeriod.getEffectiveTo();
    }

    /**
     * If any period calculation strategy defined.
     * Call getEffectiveFrom and getEffectiveTo only checking isDefined before
     */
    @Schema(hidden = true)
    public boolean isDefined() {
        return !this.findAllStrategies().isEmpty();
    }

    List<String> findAllStrategies() {

        ArrayList<String> strategies = new ArrayList<>();

        if (this.getFrom() != null && this.getTo() != null)
            strategies.add(FROM_AND_TO);

        if (this.getOneDay() != null)
            strategies.add(ONE_DAY);

        if (this.getOneWeek() != null)
            strategies.add(ONE_WEEK);

        if (this.getOneMonth() != null)
            strategies.add(ONE_MONTH);

        if (this.getOneQuarter() != null)
            strategies.add(ONE_QUARTER);

        if (this.getLastDays() != null)
            strategies.add(LAST_DAYS);

        if (this.getLastWeeks() != null)
            strategies.add(LAST_WEEKS);

        if (this.getLastMonths() != null)
            strategies.add(LAST_MONTHS);

        if (this.getLastQuarters() != null)
            strategies.add(LAST_QUARTERS);

        return strategies;
    }

    private void buildEffectivePeriod() {
        List<String> allStrategies = this.findAllStrategies();

        if (allStrategies.isEmpty())
            throw new IllegalStateException("There is not period calculation strategy defined");

        String s = allStrategies.get(0);
        Strategy strategy = this.strategyMap.get(s);

        if (strategy == null)
            throw new IllegalStateException("Strategy " + s + " is not implemented yet");

        this.effectivePeriod = strategy.buildPeriod(this);
    }

    @Override
    public String toString() {
        return "PeriodFilterForm{" +
                (this.isDefined() ? "effectivePeriod=" + this.getEffectiveFrom() + "..." + this.getEffectiveTo() + ", " : "") +
                (from == null ? "" : ", from=" + from) +
                (to == null ? "" : ", to=" + to) +
                (zone == null ? "" : ", zone=" + zone) +
                (oneDay == null ? "" : ", oneDay=" + oneDay) +
                (oneWeek == null ? "" : ", oneWeek=" + oneWeek) +
                (oneMonth == null ? "" : ", oneMonth=" + oneMonth) +
                (oneQuarter == null ? "" : ", oneQuarter=" + oneQuarter) +
                (lastDays == null ? "" : ", lastDays=" + lastDays) +
                (lastWeeks == null ? "" : ", lastWeeks=" + lastWeeks) +
                (lastMonths == null ? "" : ", lastMonths=" + lastMonths) +
                (lastQuarters == null ? "" : ", lastQuarters=" + lastQuarters) +
                '}';
    }

    /**
     * Builds from-to basic object for calculate effective from-to
     */
    static class EffectivePeriod {

        private final LocalDate from;
        private final LocalDate to;
        private final ZoneId zone;

        public EffectivePeriod(LocalDate from, LocalDate to, ZoneId zone) {
            this.from = from;
            this.to = to;
            this.zone = zone;
        }

        public Instant getEffectiveFrom() {
            LocalDateTime localDateTime = this.from.atStartOfDay();
            return localDateTime.toInstant(this.zone.getRules().getOffset(localDateTime));
        }

        public Instant getEffectiveTo() {
            LocalDateTime localDateTime = this.to.atTime(23,59,59,999999999);
            return localDateTime.toInstant(this.zone.getRules().getOffset(localDateTime));
        }
    }

    interface Strategy {
        EffectivePeriod buildPeriod(PeriodFilterForm form);
    }

}
