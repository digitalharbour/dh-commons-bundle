package com.teamharbour.commons;

import java.util.Locale;


/**
 * Context session detector
 */
public interface EnvironmentContextLocaleDetector {

    /**
     * Detect session using context
     *
     * @param context context
     * @return session string (example: en-US)
     */
    Locale detectLocale(EnvironmentContext context);
}
