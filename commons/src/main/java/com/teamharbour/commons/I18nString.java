package com.teamharbour.commons;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.*;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.lang.String.format;

@JsonInclude(JsonInclude.Include.NON_ABSENT)
public class I18nString {

    // see: https://stackoverflow.com/questions/15921332/cleaner-way-to-check-if-a-string-is-iso-country-of-iso-language-in-java?rq=1
    private static final Set<String> ISO_LANGUAGES = new HashSet<>
            (Arrays.asList(Locale.getISOLanguages()));
    private static final Set<String> ISO_COUNTRIES = new HashSet<>
            (Arrays.asList(Locale.getISOCountries()));

    /**
     * Validates if language is ISO language using {@link Locale#getISOLanguages()}.
     * E.g. "en" is ok, but "EN" or "eng" is not ok!
     * @param s language code
     * @return true if code is correct
     */
    public static boolean isValidISOLanguage(String s) {
        return ISO_LANGUAGES.contains(s);
    }

    /**
     * Validates if country code is ISO code using {@link Locale#getISOCountries()}.
     * E.g. "en" is ok, but "EN" or "eng" is not ok!
     * @param s language code
     * @return true if code is correct
     */
    public static boolean isValidISOCountry(String s) {
        return ISO_COUNTRIES.contains(s);
    }


    private final HashMap<String, String> values;
    private String defaultLang;

    @JsonCreator
    private I18nString(
            @JsonProperty("values") HashMap<String,String> values,
            @JsonProperty("defaultLang") String defaultLang
    ) {
        if (checkNotNull(values).isEmpty())
            throw new IllegalArgumentException("Unable to build I18nString with no values");
        this.values = values;
        this.defaultLang = defaultLang;
        if (this.defaultLang == null)
            this.defaultLang = this.values.keySet().stream().sorted().findFirst().orElse(Locale.getDefault().getLanguage());
    }

    public Map<String, String> getValues() {
        return Collections.unmodifiableMap(values);
    }

    public String getDefaultLang() {
        return defaultLang;
    }

    public String toString() {
        return this.values.get(this.defaultLang);
    }

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    public String getDefaultValue() {
        return this.values.get(this.defaultLang);
    }

    public String getValue(String lang) {
        return this.values.get(lang);
    }

    /**
     * Build helper. Use it instead of constructor:
     * new I18nString.Builder().add("en", "Time").add("de", "Zeit").add("uk", "Час").build();
     */
    public static class Builder {

        private HashMap<String,String> values = new HashMap<>();
        private String defaultLang;

        /**
         * Adds value and forces it as default value (otherwise, first added value will be default value)
         * @param lang language
         * @param valueForThisLang value for this language
         * @return builder object
         */
        public Builder addAsDefault(String lang, String valueForThisLang) {
            this.defaultLang = lang;
            return this.add(lang, valueForThisLang);
        }

        /**
         * Adds value for multilingual string for given language.
         * First value added will be default value if {@link #addAsDefault(String, String)}  not invoked then.
         * @param lang language
         * @param valueForThisLang value for this language
         * @return builder
         */
        public Builder add(String lang, String valueForThisLang) {
            if (!isValidISOLanguage(lang)) throw new IllegalArgumentException(format("Language '%s' is not valid ISO language", lang));
            if (defaultLang == null) defaultLang = lang;
            this.values.put(lang, valueForThisLang);
            return this;
        }

        /**
         * @return built new I18nString
         */
        public I18nString build() {
            return new I18nString(values, defaultLang);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        I18nString that = (I18nString) o;

        if (!values.equals(that.values)) return false;
        return defaultLang.equals(that.defaultLang);
    }

    @Override
    public int hashCode() {
        int result = values.hashCode();
        result = 31 * result + defaultLang.hashCode();
        return result;
    }
}
