package com.teamharbour.commons;


import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.List;

import static java.lang.String.format;

public class PeriodFilterFormValidator implements ConstraintValidator<ValidPeriodFilterForm, PeriodFilterForm> {

    @Override
    public boolean isValid(PeriodFilterForm value, ConstraintValidatorContext context) {

        // null is ok, use @NotNull constraint
        if (value == null)
            return true;

        // disable default constraint violation
        context.disableDefaultConstraintViolation();

        List<String> allStrategies = value.findAllStrategies();

        // looks like there is no filtering
        if (allStrategies.isEmpty()) {
            context.buildConstraintViolationWithTemplate("there is no period calculation parameters defined")
                    .addConstraintViolation();
            return false;
        }

        // only one of filters must be there
        if (allStrategies.size() > 1) {
            context.buildConstraintViolationWithTemplate("more than one period filter detected at the same time:  " + String.join(", ", allStrategies))
                    .addConstraintViolation();
            return false;
        }

        // from -> to (if FROM is greater than TO, error; equal is ok)
        if (allStrategies.get(0).equals(PeriodFilterForm.FROM_AND_TO) && value.getFrom().compareTo(value.getTo()) > 0)
        {
            context.buildConstraintViolationWithTemplate(format("'to' date '%s' must be after 'from' date '%s'", value.getTo(), value.getFrom()))
                    .addConstraintViolation();
            return false;
        }

        //

        return true;
    }


}
