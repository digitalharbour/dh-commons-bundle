package com.teamharbour.commons;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Strings;
import com.google.common.io.CharStreams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.lang.String.format;

public class ConfigurationLoader {

    /**
     * "DH_HOME" as default env variable for home directory
     */
    // do not delete it cause it used in project dh-server heavily
    public static final String ENV_VAR_DH_HOME = "DH_HOME";

    private Path homeDir;
    private Class callerClassForLoadResources;
    protected Logger logger;

    private ConfigurationLoader(Class callerClassForLoadResources) {
        this.logger = LoggerFactory.getLogger(this.getClass());
        this.callerClassForLoadResources = checkNotNull(callerClassForLoadResources);
    }

    public ConfigurationLoader(String homeEnvVarName, Class callerClassForLoadResources) {
        this(checkNotNull(callerClassForLoadResources)); // init logger
        String homeDirStr = System.getenv(checkNotNull(homeEnvVarName));
        if (Strings.isNullOrEmpty(homeDirStr))
            throw new IllegalArgumentException("homeEnvVarName must be set to env variable, having not empty value but " + homeEnvVarName + " is empty");
        this.homeDir = Paths.get(homeDirStr);
        this.initHomeDir();
    }

    public ConfigurationLoader(@NotNull Path homeDir, Class callerClassForLoadResources) {
        this(checkNotNull(callerClassForLoadResources));
        this.homeDir = homeDir;
        this.initHomeDir();
    }

    private void initHomeDir() {
        if (!this.homeDir.toFile().exists()) {
            logger.info("Home directory '{}' not exists, creating...", this.homeDir);
            try {
                Files.createDirectory(this.homeDir);
            } catch (IOException e) {
                throw new IllegalArgumentException(format("Unable to create directory '%s'", this.homeDir), e);
            }
            logger.info("Home directory '{}' created successfully", this.homeDir);
        }
        else if (!this.homeDir.toFile().isDirectory())
            throw new IllegalStateException("Path " + this.homeDir + " is not exists or not a directory");

        this.logger.info("Config loader created for '{}' with home dir '{}'", callerClassForLoadResources.getSimpleName(), this.homeDir);
    }

    public Path getHomeDir() {
        return this.homeDir;
    }


    /**
     * Loads config as input stream.
     * It tries to find file in home dir or copy it from resources
     * @param configResourceName path from home dir as resource name or just resource name
     * @return config file input stream
     * @throws IOException if it's unable to open file or unable to copy it from resources
     */
    public InputStream loadConfig(String configResourceName) throws IOException {

        Path configPath = this.homeDir.resolve(checkNotNull(configResourceName));

        if (!configPath.toFile().exists()) {
            logger.info("File '{}' not exists, creating it from resources...", configPath);

            InputStream resourceInput = this.callerClassForLoadResources.getClassLoader().getResourceAsStream(configResourceName);
            if (resourceInput == null)
                throw new IllegalArgumentException(format("Resource '%s' does not exists", configResourceName));

            if (!configPath.getParent().toFile().exists()) {
                logger.info("File '{}' need to create directories '{}'", configPath, configPath.getParent());
                Files.createDirectories(configPath.getParent());
            }

            Files.copy(resourceInput,configPath,StandardCopyOption.REPLACE_EXISTING);

            logger.info("File '{}' created successfully", configPath);
        }
        return Files.newInputStream(configPath, StandardOpenOption.READ);
    }

    /**
     * Loads as UTF-8 string of {@link #loadConfig(String)}
     * @param configSubPath path from home dir as resource name
     * @return config content as UTF-8 text
     * @throws IOException if it's unable to open file or unable to copy it from resources
     */
    public String loadConfigAsText(String configSubPath) throws IOException {
        try (InputStream input = this.loadConfig(checkNotNull(configSubPath))) {
            return CharStreams.toString(new InputStreamReader(input, StandardCharsets.UTF_8));
        }
    }

    /**
     * Loads configuration as JsonNode (Jackson)
     * with default mapper.
     *
     * @param configSubPath resource name like "my-config.json"
     * @return json node as from standard object mapper
     * @throws IOException in case if path is wrong or content of resource
     *                     can't be parsed as JSON
     */
    public JsonNode loadConfigAsJson(String configSubPath) throws IOException {
        return this.loadConfigAsJson(checkNotNull(configSubPath), new ObjectMapper().findAndRegisterModules());
    }

    /**
     * Loads configuration as JsonNode (Jackson)
     * with custome configured mapper.
     *
     * @param configSubPath resource name like "my-config.json"
     * @param mapper configured mapper
     * @return json node as from standard object mapper
     * @throws IOException in case if path is wrong or content of resource
     *                     can't be parsed as JSON
     */
    public JsonNode loadConfigAsJson(String configSubPath, ObjectMapper mapper) throws IOException {
        String text = loadConfigAsText(checkNotNull(configSubPath));
        return checkNotNull(mapper).readTree(text);
    }

}
