package com.teamharbour.commons;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_ABSENT)
public class CommonCategory {

    private String id;
    private final I18nString title;

    @JsonCreator
    public CommonCategory(
            @JsonProperty("id") String id,
            @JsonProperty("title") I18nString title
    )
    {
        this.id = id;
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public I18nString getTitle() {
        return title;
    }
}
