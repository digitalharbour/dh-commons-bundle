package com.teamharbour.commons;

import com.fasterxml.jackson.annotation.JsonView;

import java.util.Collection;

/**
 * Simple version of JsonListWrapper.
 * Usually, it is preferable choice cause this version is easy understandable by any tools like SpringDoc (OpenAPI), etc.
 * However, it is limited by just wrapping list into single element "list": no "list" element name variations, no additional properties.
 * @param <T> type of list items
 */
@JsonView(ViewProfile.Minimal.class) // only parameter list must always be present
public class SimpleJsonListWrapper<T> {

    private Collection<T> list;

    public SimpleJsonListWrapper(Collection<T> list) {
        this.list = list;
    }

    public Collection<T> getList() {
        return list;
    }
}
