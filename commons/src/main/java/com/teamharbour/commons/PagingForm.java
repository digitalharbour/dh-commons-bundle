package com.teamharbour.commons;

import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import static java.util.Optional.ofNullable;

@Schema(title = "Paging parameters")
public class PagingForm {

    public static final int DEFAULT_LIMIT = 20;

    @Positive
    @NotNull
    private Integer page = 1;

    @Positive
    @NotNull
    private Integer limit = DEFAULT_LIMIT;

    public PagingForm() {
        // empty for building automatically e.g. from spring-controller
    }

    public PagingForm(Integer page, Integer limit) {
        this.page = ofNullable(page).orElse(1);
        this.limit = ofNullable(limit).orElse(DEFAULT_LIMIT);
    }

    @Schema(description = "page number, started from 1; cannot be 0 or negative; 1 by default")
    public Integer getPage() {
        return this.page;
    }

    @Schema(description = "item limit per page; cannot be 0 or negative; 10 by default")
    public Integer getLimit() {
        return this.limit;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    @Override
    public String toString() {
        return "PagingForm{" +
                "page=" + page +
                ", limit=" + limit +
                '}';
    }
}
