package com.teamharbour.commons;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Properties;

@JsonInclude(JsonInclude.Include.NON_ABSENT)
public class GitInfo {

    public final String dirty;
    public final String remoteOriginURL;
    public final String tags;
    public final String totalCommitCount;
    public final Commit commit = new Commit();
    public final String closestTagCommitCount;
    public final String closestTagName;
    public final String branch;
    public final Build build = new Build();

    public class Build {

        public String host;
        public String time;
        public User user = new User();
        public String version;
    }

    public class Commit {

        public String id;
        public String abbrev;
        public String describe;
        public String messageFull;
        public String messageShort;
        public String time;
        public String userEmail;
        public String userName;
    }

    public class User {

        public String email;
        public String name;
    }


    /**
     * Instantiate from com.gorylenko.gradle-git-properties plugin "git.properties":
     * git.branch
     * git.build.host
     * git.build.time
     * git.build.user.email
     * git.build.user.name
     * git.build.version
     * git.closest.tag.commit.count
     * git.closest.tag.name
     * git.commit.id
     * git.commit.id.abbrev
     * git.commit.id.describe
     * git.commit.message.full
     * git.commit.message.short
     * git.commit.time
     * git.commit.user.email
     * git.commit.user.name
     * git.dirty
     * git.remote.origin.url
     * git.tags
     * git.total.commit.count
     */
    public GitInfo(Properties props) {
        this.branch = props.getProperty("git.branch");
        this.build.host = props.getProperty("git.build.host");
        this.build.time = props.getProperty("git.build.time");
        this.build.user.email = props.getProperty("git.build.user.email");
        this.build.user.name = props.getProperty("git.build.user.name");
        this.build.version = props.getProperty("git.build.version");
        this.closestTagCommitCount = props.getProperty("git.closest.tag.commit.count");
        this.closestTagName = props.getProperty("git.closest.tag.name");
        this.commit.id = props.getProperty("git.commit.id");
        this.commit.abbrev = props.getProperty("git.commit.id.abbrev");
        this.commit.describe = props.getProperty("git.commit.id.describe");
        this.commit.messageFull = props.getProperty("git.commit.message.full");
        this.commit.messageShort = props.getProperty("git.commit.message.short");
        this.commit.time = props.getProperty("git.commit.time");
        this.commit.userEmail = props.getProperty("git.commit.user.email");
        this.commit.userName = props.getProperty("git.commit.user.name");
        this.dirty = props.getProperty("git.dirty");
        this.remoteOriginURL = props.getProperty("git.remote.origin.url");
        this.tags = props.getProperty("git.tags");
        this.totalCommitCount = props.getProperty("git.total.commit.count");
    }

    public String getBuildVersion() {
        return this.build.version;
    }

    public String getCommitId() {
        return this.commit.id;
    }
}
