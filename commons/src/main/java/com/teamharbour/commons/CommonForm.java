package com.teamharbour.commons;

/**
 * Just root for any Form objects used to manage some applications, orders, commands, any entities which may use forms.
 * Common form allows to get any name-&gt;value json object and obtain it as hashmap (extends AnyAttributesObject)
 * It is very useful to extend many business-logic form from this root to get
 * free extension point for any additional attributes.
 */
public class CommonForm extends AnyAttributesObject {

}
