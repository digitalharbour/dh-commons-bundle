package com.teamharbour.commons;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.*;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * List wrapper for JSON practice serializing root list with some root entry:
 * <pre>{list: [...]}</pre>
 * instead of just
 * <pre>[]</pre>
 * @param <T> type of list items
 */
public class JsonListWrapper<T> {

    public static final String DEFAULT_LIST_ENTRY_NAME = "list";

    private HashMap<String, Object> properties = new HashMap<>();
    private ArrayList<T> list = new ArrayList<>();
    private String currentListEntryName = DEFAULT_LIST_ENTRY_NAME;

    public JsonListWrapper(T... items) {
        this(Arrays.asList(items));
    }

    @JsonCreator
    public JsonListWrapper(@JsonProperty("list") Collection<T> items) {
        this.list.addAll(checkNotNull(items));
        this.properties.put(this.currentListEntryName, this.list);
    }

    public JsonListWrapper<T> listEntryName(String listName) {
        this.properties.remove(this.currentListEntryName);
        this.currentListEntryName = listName;
        this.properties.put(this.currentListEntryName, this.list);
        return this;
    }

    public JsonListWrapper<T> withCount() {
        this.properties.put("count", this.list.size());
        return this;
    }

    @JsonIgnore
    public List<T> getList() {
        return this.list;
    }

    @JsonAnyGetter
    public Map<String, Object> getProperties() {
        return this.properties;
    }

}
