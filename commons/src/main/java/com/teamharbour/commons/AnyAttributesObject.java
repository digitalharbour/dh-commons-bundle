package com.teamharbour.commons;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Allows to get any name-&gt;value json object and obtain it as map.
 * It is very useful to extend many business-logic form from this root to get
 * free extension point for any additional attributes.
 */
@JsonInclude(JsonInclude.Include.NON_ABSENT)
public class AnyAttributesObject {
    private Map<String, String> attributes = new LinkedHashMap<>();

    @JsonAnySetter
    public void putAttribute(String key, String value) {
        this.attributes.put(key, value);
    }

    @JsonAnyGetter
    public Map<String,String> getAttributes() {
        return Collections.unmodifiableMap(this.attributes);
    }

}
