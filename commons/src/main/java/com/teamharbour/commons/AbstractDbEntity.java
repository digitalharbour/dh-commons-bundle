package com.teamharbour.commons;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.Objects;
import java.util.UUID;

/**
 * Abstract entity for all domain persisted classes.
 * Inspired by: http://blog.xebia.com/jpa-implementation-patterns-using-uuids-as-primary-keys/
 */
@MappedSuperclass
public class AbstractDbEntity extends AbstractEntity {


    /**
     * Every time we modify entity it changes it's updatedAt attribute
     * (after it is persisted)
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @Column(name = "updated_at", nullable = false, columnDefinition = "TIMESTAMP")
    @Convert(converter = JPAInstantConverter.class)
    @NotNull
    private Instant updatedAt;

    public AbstractDbEntity() {
        super(UUID.randomUUID().toString(), Instant.now());
        this.updatedAt = Instant.now();
    }

    @Schema(description = "last updated timestamp", required = true)
    public Instant getUpdatedAt() {
        return updatedAt;
    }

    // check https://docs.jboss.org/hibernate/orm/4.0/hem/en-US/html/listeners.html for all states
    // Persist vs Update: http://www.baeldung.com/hibernate-save-persist-update-merge-saveorupdate

    /**
     *
     */
    @PrePersist
    @PreUpdate
    protected void preSaveOrUpdate() {
        this.updatedAt = Instant.now();
    }

    /**
     * Override it if want init some transient fields (e.g. calculate age by birth date, etc.)
     */
    @PostLoad
    protected void postLoad() {
        // this method could be overrided
    }

}
