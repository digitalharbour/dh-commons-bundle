package com.teamharbour.commons;

public class Documentation {

    String version;
    String documentation;

    public Documentation(String version, String documentation) {
        this.version = version;
        this.documentation = documentation;
    }

    public String getVersion() {
        return version;
    }

    public String getDocumentation() {
        return documentation;
    }
}