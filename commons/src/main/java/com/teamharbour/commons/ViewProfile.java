package com.teamharbour.commons;

public class ViewProfile {
    /**
     * For special set of fields
     */
    public interface Special {}

    /**
     * Minimal view like just ID or ID+NAME
     */
    public interface Minimal {}

    /**
     * Basic view like all fields except lazy initialized or inherited structures
     */
    public interface Basic extends Minimal{}

    /**
     * Maximum view
     */
    public interface Full extends Basic {}

    public interface BasicWithSpecial extends Basic, Special {}

    public interface MinimalWithSpecial extends Minimal, Special {}

    public interface FullWithSpecial extends Full, Special {}
}
