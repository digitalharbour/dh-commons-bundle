package com.teamharbour.digitalharbour;

import com.teamharbour.commons.ConfigurationLoader;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.stream.Stream;

// todo: move it to commons-mocks but current mocks move to dh-mocks
public class MockEnvConfigurationLoader extends ConfigurationLoader {


    public static final String HOME_DIR_PREFIX = "dh_test_";

    /**
     *
     * @param pathFromHomeOrTmp use it as path from $HOME or $TMP
     */
    public MockEnvConfigurationLoader(String pathFromHomeOrTmp, Class callerClassForLoadResources) {
        super(createTempDirectoryNoCheckedExceptions(pathFromHomeOrTmp), callerClassForLoadResources);
    }

    public MockEnvConfigurationLoader(Class callerClassForLoadResources) {
        this(HOME_DIR_PREFIX, callerClassForLoadResources);
    }

    private static Path createTempDirectoryNoCheckedExceptions(String prefix) {
        try {
            return Files.createTempDirectory(prefix);
        } catch (IOException e) {
            throw new IllegalStateException("Unable to create temp directory with jvm", e);
        }
    }

    /**
     * Force purging home dir
     */
    public void purgeHomeDir() throws IOException {
        this.logger.info("{} is purging {}", this, this.getHomeDir());
        try (Stream<Path> stream = Files.walk(this.getHomeDir())) {
            stream.sorted(Comparator.reverseOrder())
                    .map(Path::toFile)
                    .forEach(File::delete);
        }
        //Files.delete(this.getHomeDir()); // todo: not works on Windows, have no idea why it throws java.nio.file.DirectoryNotEmptyException
    }

    /**
     * For Spring bean shutdown if no destroyMethod mentioned
     */
    public void close() {
        try {
            this.logger.info("{} is closing", this);
            this.purgeHomeDir();
        } catch (IOException e) {
            this.logger.error("Unable to close " + this, e);
        }
    }

}
