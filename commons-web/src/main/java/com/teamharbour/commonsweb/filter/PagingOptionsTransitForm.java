package com.teamharbour.commonsweb.filter;


import com.teamharbour.commons.PagingOptions;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import static com.teamharbour.commons.EntityFilter.ALL_LIMIT;
import static com.teamharbour.commons.PagingOptions.createOffsetAndLimit;
import static com.teamharbour.commons.PagingOptions.createPageAndLimit;
import static com.teamharbour.commons.exceptions.CommonUserException.BAD_PARAMETER_RANGE;

public class PagingOptionsTransitForm {

    @Min(value = 1, message = BAD_PARAMETER_RANGE) @Max(value = 1000, message = BAD_PARAMETER_RANGE)
    private Integer limit;

    @Min(value = 1, message = BAD_PARAMETER_RANGE)
    private Integer page;

    @Min(value = 0, message = BAD_PARAMETER_RANGE)
    private Integer offset;

    PagingOptionsTransitForm(Integer limit, Integer page, Integer offset) {
        this.limit = limit;
        this.page = page;
        this.offset = offset;
    }

    public Integer getLimit() {
        return limit;
    }

    public Integer getPage() {
        return page;
    }

    public Integer getOffset() {
        return offset;
    }

    public PagingOptions toPagingOptions() {
        // Offset has higher priority than limit
        if (offset != null) {
            return createOffsetAndLimit(offset, limit);
        } else if (page != null) {
            return createPageAndLimit(page, limit);
        } else if (limit != null) {
            return createPageAndLimit(1, limit);
        } else {
            return createOffsetAndLimit(0, ALL_LIMIT);
        }
    }
}
