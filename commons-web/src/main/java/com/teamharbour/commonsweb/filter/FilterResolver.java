package com.teamharbour.commonsweb.filter;

import com.teamharbour.commons.EntityFilter;
import com.teamharbour.commons.PagingOptions;
import com.teamharbour.commons.exceptions.BadParamUserException;
import com.teamharbour.commons.exceptions.CommonUserException;
import com.teamharbour.commons.exceptions.ValidationUserException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.lang.annotation.Annotation;
import java.time.Instant;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static java.lang.Boolean.valueOf;
import static java.lang.Integer.parseInt;
import static java.lang.String.format;
import static java.util.Optional.*;

public abstract class FilterResolver<A extends Annotation, F extends EntityFilter> implements HandlerMethodArgumentResolver {

    private final Logger logger;

    private final Integer defaultFilterLimit;

    private final String PARAM_LIMIT = "limit";

    private final String PARAM_PAGE = "page";

    private final String PARAM_OFFSET = "offset";

    private final Validator validator;

    /**
     * Use this field in descendants to access request's additional parameters
     */
    private NativeWebRequest request;


    public FilterResolver(Integer defaultFilterLimit) {
        this.logger = LoggerFactory.getLogger(this.getClass());
        this.defaultFilterLimit = defaultFilterLimit;

        ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
        validator = validatorFactory.getValidator();
    }

    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        return parameter.getParameterAnnotation(parameterAnnotation()) != null;
    }

    /**
     * Parameter annotation class
     *
     * @return class
     */
    protected abstract Class<A> parameterAnnotation();

    @Override
    public Object resolveArgument(
            MethodParameter parameter,
            ModelAndViewContainer mavContainer,
            NativeWebRequest webRequest,
            WebDataBinderFactory binderFactory) throws ValidationUserException {

        // Save request to possible usage in descendants
        this.request = webRequest;

        List<CommonUserException> errors = new ArrayList<>();

        // Validate incoming limit, page, offset (all of them must be Integer)
        Integer limit = validateAndGet(webRequest.getParameter(PARAM_LIMIT), errors, PARAM_LIMIT);
        Integer page = validateAndGet(webRequest.getParameter(PARAM_PAGE), errors, PARAM_PAGE);
        Integer offset = validateAndGet(webRequest.getParameter(PARAM_OFFSET), errors, PARAM_OFFSET);

        // Page or offset
        if (page != null && offset != null) {
            throw new ValidationUserException("only_page_or_offset", "Only page or offset allowed");
        }

        // Validate limit, page, offset
        PagingOptionsTransitForm form = new PagingOptionsTransitForm(limit, page, offset);
        Set<ConstraintViolation<Object>> constraintViolations = validator.validate(form);

        // Throw validation exception in case of validation errors
        if (!errors.isEmpty() || !constraintViolations.isEmpty()) {
            throw new ValidationUserException("Bad filter parameters")
                    .addConstraintViolations(errors)
                    .addConstraintViolations(constraintViolations);
        }

        // Set default value if limit wasn't set
        if (limit == null) {
            logger.debug("No limit parameter in request. Using default limit: {}", defaultFilterLimit);
            limit = defaultFilterLimit;
        }

        PagingOptions pagingOptions = new PagingOptionsTransitForm(limit, form.getPage(), form.getOffset()).toPagingOptions();

        return createFilter(pagingOptions);
    }

    /**
     * Provide your entity filter here
     *
     * @param pagingOptions paging options
     * @return entity filter
     * @throws ValidationUserException if paging options not valid
     */
    protected abstract F createFilter(PagingOptions pagingOptions) throws ValidationUserException;

    private Integer validateAndGet(String param, List<CommonUserException> errors, String paramName) {
        if (param == null) {
            return null;
        }

        try {
            return parseInt(param);
        } catch (NumberFormatException e) {
            errors.add(new BadParamUserException.BadPositiveIntegerBuilder(paramName, param).build());
            return null;
        }
    }

    protected Optional<String> getRequestStringParameter(String paramName) {
        return ofNullable(request.getParameter(paramName));
    }

    protected Optional<Integer> getRequestIntegerParameter(String paramName) {
        try {
            return of(Integer.valueOf(request.getParameter(paramName)));
        } catch (NumberFormatException nfe) {
            logger.warn(format("Param is not an integer, but :%s", request.getParameter(paramName)));
            return empty();
        }
    }

    protected Optional<Boolean> getRequestBooleanParameter(String paramName) {
        String parameter = request.getParameter(paramName);
        if ("true".equalsIgnoreCase(parameter) || "false".equalsIgnoreCase(parameter)) {
            return of(valueOf(parameter));
        } else {
            return empty();
        }
    }

    protected Optional<Instant> getRequestInstantParameter(String paramName) {
        String parameter = request.getParameter(paramName);
        if (parameter != null) {
            try {
                return of(Instant.parse(parameter));
            } catch (DateTimeParseException e) {
                logger.warn("Bad instant parameter", e);
                return empty();
            }
        } else {
            return empty();
        }
    }

    protected void setupFilterDates(EntityFilter filter) {
        Optional<Instant> createdFrom = getRequestInstantParameter("createdFrom");
        Optional<Instant> createdTo = getRequestInstantParameter("createdTo");
        Optional<Integer> createdLastDays = getRequestIntegerParameter("createdLastDays");
        Optional<Instant> modifiedFrom = getRequestInstantParameter("modifiedFrom");
        Optional<Instant> modifiedTo = getRequestInstantParameter("modifiedTo");
        Optional<Integer> modifiedLastDays = getRequestIntegerParameter("modifiedLastDays");

        // Created
        if (createdLastDays.isPresent()) {
            createdLastDays.ifPresent(filter::setCreatedLastDays);
        } else {
            if (createdFrom.isPresent() && createdTo.isPresent()) {
                createdFrom.ifPresent(filter::setCreatedFrom);
                createdTo.ifPresent(filter::setCreatedTo);
            }
        }

        // Updated
        if (modifiedLastDays.isPresent()) {
            modifiedLastDays.ifPresent(filter::setUpdatedLastDays);
        } else {
            if (modifiedFrom.isPresent() && modifiedTo.isPresent()) {
                modifiedFrom.ifPresent(filter::setUpdatedFrom);
                modifiedTo.ifPresent(filter::setUpdatedTo);
            }
        }
    }
}