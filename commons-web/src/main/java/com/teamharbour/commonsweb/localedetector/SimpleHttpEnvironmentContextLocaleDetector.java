package com.teamharbour.commonsweb.localedetector;


import com.teamharbour.commonsweb.HttpEnvironmentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletRequest;
import java.util.Enumeration;
import java.util.Locale;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.lang.String.format;

/**
 * Simple session detector based on
 *
 * @see ServletRequest#getLocales()
 * <p>
 * Gets first available session ignoring the rest (but logs them).
 */
public class SimpleHttpEnvironmentContextLocaleDetector extends HttpEnvironmentContextLocaleDetector {

    private final Logger logger;

    public SimpleHttpEnvironmentContextLocaleDetector() {
        logger = LoggerFactory.getLogger(this.getClass());
    }

    @Override
    protected Locale detectLocaleByHttpContext(HttpEnvironmentContext context) {
        Enumeration<Locale> locales = checkNotNull(context).getRequest().getLocales();

        Locale first = null;
        while (locales.hasMoreElements()) {
            Locale locale = locales.nextElement();

            if (first == null) {
                first = locale;
                logger.debug("Selected locale: {}", first);
            } else {
                if (logger.isDebugEnabled()) {
                    logger.debug("Available locale: {}", locale.toString());
                }
            }
        }

        if (first == null) {
            first = Locale.getDefault();
            logger.warn("Unable to detect locale from {}, using default system: {}", context, first);
        }

        return first;
    }

    @Override
    public String toString() {
        return format("Simple HTTP Locale Detector. Class name: %s", this.getClass().getName());
    }
}
