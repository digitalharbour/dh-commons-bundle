package com.teamharbour.commonsweb.localedetector;

import com.teamharbour.commons.EnvironmentContext;
import com.teamharbour.commons.EnvironmentContextLocaleDetector;
import com.teamharbour.commonsweb.HttpEnvironmentContext;

import java.util.Locale;

import static java.lang.String.format;

/**
 * Detector based on HttpEnvironmentContext.
 * <p>
 * Requires HttpEnvironmentContext as context (otherwise throws exception)
 */
public abstract class HttpEnvironmentContextLocaleDetector implements EnvironmentContextLocaleDetector {

    @Override
    public Locale detectLocale(EnvironmentContext context) {

        if (!(context instanceof HttpEnvironmentContext)) {
            throw new IllegalArgumentException(format("Context must be HttpEnvironmentContext, but was: %s", context.toString()));
        }

        return detectLocaleByHttpContext((HttpEnvironmentContext) context);
    }

    /**
     * Return string representation of Locale
     *
     * @param httpEnvironmentContext context
     * @return session string (example: en-US)
     */
    protected abstract Locale detectLocaleByHttpContext(HttpEnvironmentContext httpEnvironmentContext);

    @Override
    public String toString() {
        return format("Environment Context Locale Detector type: %s", this.getClass().getName());
    }
}
