package com.teamharbour.commonsweb;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.teamharbour.commons.EnvironmentContext;

import javax.servlet.http.HttpServletRequest;

import static java.util.Collections.list;
import static java.util.Optional.ofNullable;

/**
 * Http environment context should be provided for sessions from http controllers.
 * HttpEnvironmentContext should give full information about HTTP specific environment:
 * requested uri, address, headers, parameters.
 */
@JsonIgnoreProperties(value = { "request" })
public class HttpEnvironmentContext extends EnvironmentContext {

    private final HttpServletRequest request;

    private static final String ATTR_NAME_REMOTE_ADDR = "remote-addr";
    private static final String ATTR_NAME_REQ_URI = "req-uri";
    private static final String ATTR_NAME_REMOTE_USER = "remote-user";
    private static final String ATTR_NAME_USER_AGENT = "user-agent";
    private static final String HTTP_HEADER_PREFIX = "http-header-";
    private static final String HTTP_PARAMETER_PREFIX = "http-param-";


    private static final String HTTP_HEADER_USER_AGENT = "User-Agent";
    // https://en.wikipedia.org/wiki/X-Forwarded-For#Format
    public static final String HTTP_HEADER_X_FORWARDED_FOR = "X-Forwarded-For";
    // https://docs.cloud.oracle.com/iaas/Content/Balance/Reference/httpheaders.htm
    public static final String HTTP_HEADER_X_REAL_IP = "X-Real-IP";


    public HttpEnvironmentContext(HttpServletRequest request) {

        this.request = request;

        String ipAddress = request.getHeader(HTTP_HEADER_X_REAL_IP);

        if (ipAddress == null)
            ipAddress = request.getHeader(HTTP_HEADER_X_FORWARDED_FOR);

        if (ipAddress == null)
            ipAddress = request.getRemoteAddr();

        // For case there are several IP addresses like in X-Forwarded-For, get the first onw
        ipAddress = ipAddress.split(",")[0].trim();

        setAttribute(ATTR_NAME_REMOTE_ADDR, ipAddress);

        setAttribute(ATTR_NAME_REQ_URI, request.getRequestURI());

        setAttribute(ATTR_NAME_USER_AGENT, request.getHeader(HTTP_HEADER_USER_AGENT));

        String remoteUser = request.getRemoteUser();
        setAttribute(ATTR_NAME_REMOTE_USER, remoteUser == null ? "anonymous" : remoteUser);

        list(request.getHeaderNames())
                .forEach(name -> setAttribute(HTTP_HEADER_PREFIX + name, request.getHeader(name)));

        list(request.getParameterNames())
                .forEach(name -> setAttribute(HTTP_PARAMETER_PREFIX + name, request.getParameter(name)));
    }

    public String getRemoteAddress() {
        return getAttribute(ATTR_NAME_REMOTE_ADDR);
    }

    public String getRequestUri() {
        return getAttribute(ATTR_NAME_REQ_URI);
    }

    public String getRemoteUser() {
        return getAttribute(ATTR_NAME_REMOTE_USER);
    }

    public String getHeader(String name) {
        return getAttribute(HTTP_HEADER_PREFIX + name);
    }

    public String getParameter(String name) {
        return getAttribute(HTTP_PARAMETER_PREFIX + name);
    }

    public HttpServletRequest getRequest() {
        return request;
    }

    @Override
    public String getUserName() {
        return this.getAttribute(ATTR_NAME_REMOTE_USER);
    }

    @Override
    public String getUserAddress() {
        return this.getAttribute(ATTR_NAME_REMOTE_ADDR);
    }

    @Override
    public String getUserAgent() {
        return this.getAttribute(ATTR_NAME_USER_AGENT);
    }
}
