package com.teamharbour.commonsweb;

import com.teamharbour.commons.ConfigurationLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.event.EventListener;

import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedHashMap;
import java.util.Map;

import static java.lang.String.format;
import static java.util.Optional.ofNullable;

/**
 * Root class for Spring Boot Application with Configuration Loader support
 */
public abstract class SpringBootApp extends SpringBootServletInitializer {

    protected Logger log = LoggerFactory.getLogger(SpringBootApp.class);

    /**
     * Start it in your main method
     *
     * @param args  args
     * @param clazz class
     */
    public void start(String[] args, Class clazz) {
        String appHome = provideAppHome();
        ConfigurationLoader configurationLoader = new ConfigurationLoader(appHome, clazz);
        String configName = provideConfigName();
        String configPropertiesName = configName + ".properties";

        try (InputStream ignored = configurationLoader.loadConfig(configPropertiesName)) {
            // ignored; just to 100% check config is really loadable
        } catch (IOException e) {
            String text = format("Unable to load main config. Application Home: [%s], Configuration Name: [%s]", appHome, configName);
            throw new IllegalStateException(text, e);
        }

        configurationLoader.getHomeDir().resolve(configPropertiesName);

        // toUri better works for Windows, Cygwin, etc.
        System.setProperty("spring.config.location", configurationLoader.getHomeDir().toUri().toString());
        System.setProperty("spring.config.name", configName);

        new SpringApplicationBuilder(clazz)
                .build()
                .run(args);
    }

    @EventListener(ApplicationReadyEvent.class)
    public void appLoaded() {
        LinkedHashMap<String, String> settings = appSettings();

        if (settings.isEmpty()) {
            return;
        }

        log.info("----------------- Application Settings List ----------------------");
        for (Map.Entry entry : settings.entrySet()) {
            log.info("{}   {}",  entry.getKey(),  ofNullable(entry.getValue()).orElse("[default]"));
        }
        log.info("----------------- Application Settings List End ------------------");
    }

    protected LinkedHashMap<String, String> appSettings() {
        return new LinkedHashMap<>();
    }

    /**
     * Set this environment variable before starting application
     *
     * @return app home
     */
    protected abstract String provideAppHome();

    /**
     * Provide configuration name
     *
     * @return configuration name
     */
    protected abstract String provideConfigName();

}