package com.teamharbour.commonsweb.resolver;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.PARAMETER)
/*
  Use annotation:
  @AuthorizationToken String sessionToken
 */
public @interface AuthorizationToken {
}