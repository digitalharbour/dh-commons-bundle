package com.teamharbour.commonsweb.resolver;

import com.teamharbour.commons.exceptions.BadParamUserException;
import com.teamharbour.commons.exceptions.ValidationUserException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import java.util.StringTokenizer;

/**
 * Gives a possibility to receive auth token using the following code:
 * <p>
 * [skipped] @AuthorizationToken String sessionToken
 */
public class AuthorizationTokenResolver implements HandlerMethodArgumentResolver {

    private final Logger logger;

    public AuthorizationTokenResolver() {
        this.logger = LoggerFactory.getLogger(this.getClass());
    }

    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        return parameter.getParameterAnnotation(AuthorizationToken.class) != null;
    }

    @Override
    public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer,
                                  NativeWebRequest webRequest,
                                  WebDataBinderFactory binderFactory) throws ValidationUserException {

        String authHeader = webRequest.getHeader("Authorization");

        if (authHeader == null) {
            throw new ValidationUserException("No Authorization header");
        }

        StringTokenizer stringTokenizer = new StringTokenizer(authHeader, " ");

        if (stringTokenizer.countTokens() != 2) {
            logger.debug("Wrong structure of a token header parameter. Header '{}'", authHeader);

            throw new  BadParamUserException.CommonBuilder("Authorization", authHeader).build();
        }

        // First chunk must be "Bearer"
        String bearer = stringTokenizer.nextToken();

        if (!"Bearer".equals(bearer)) {
            logger.debug("First part of a token doesn't contain 'Bearer'. Header '{}'", authHeader);

            throw new BadParamUserException.CommonBuilder("Bearer", bearer).build();
        }

        // Token is separated from Bearer by single whitespace
        String token = stringTokenizer.nextToken();

        logger.debug("Token: '{}'", token);

        return token;
    }
}