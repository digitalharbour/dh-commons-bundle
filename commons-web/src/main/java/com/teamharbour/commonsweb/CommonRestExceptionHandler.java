package com.teamharbour.commonsweb;


import com.teamharbour.commons.exceptions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.TypeMismatchException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingPathVariableException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.multipart.support.MissingServletRequestPartException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.List;
import java.util.stream.Collectors;

import static com.teamharbour.commons.CommonsUtils.arrayToMap;
import static com.teamharbour.commons.exceptions.CommonUserException.*;
import static java.lang.String.format;
import static java.lang.String.valueOf;
import static org.springframework.http.HttpStatus.*;

/**
 * Typical usage in Spring REST application is just:
 * <br/><br/>
 * <pre>
 * {@literal @}RestControllerAdvice
 *  public class ExceptionHandler extends CommonRestExceptionHandler {
 *
 *  }
 * </pre>
 *
 */
public class CommonRestExceptionHandler extends ResponseEntityExceptionHandler {

    protected Logger log;

    protected static final String HANDLER_MESSAGE = "{} will be handled by custom handler";

    protected static final String MESSAGE = "message";

    public CommonRestExceptionHandler() {
        this.log = LoggerFactory.getLogger(this.getClass());
    }

    @ExceptionHandler(RestClientException.class)
    protected ResponseEntity<CommonUserException> handleRestClientException(RestClientException e) {

        log.error("Rest client error occurred", e);

        // Show more details if http status code exception occurred
        if (e instanceof HttpStatusCodeException) {
            HttpStatusCodeException statusCodeException = (HttpStatusCodeException) e;
            return new ResponseEntity<>(new InternalErrorUserException(REST_CLIENT_PROBLEM, "Internal service error with code: {}",
                    arrayToMap(MESSAGE, statusCodeException.getStatusText(),
                            "http-code", valueOf(statusCodeException.getRawStatusCode()))),
                    statusCodeException.getStatusCode());
        } else {
            return new ResponseEntity<>(new InternalErrorUserException(REST_CLIENT_PROBLEM, "Internal communication problem"), INTERNAL_SERVER_ERROR);
        }
    }

    @ExceptionHandler(AuthenticationUserException.class)
    protected ResponseEntity<CommonUserException> handleUserException(AuthenticationUserException e) {
        log.debug(HANDLER_MESSAGE, e.getClass().getSimpleName());
        log.warn("Authentication fail occurred", e); // just warn (due user do wrong), not our error

        return new ResponseEntity<>(e, UNAUTHORIZED);
    }

    /**
     * General user exception handling (500)
     *
     * @param e exception
     * @return response entity with CommonUserException
     */
    @ExceptionHandler(CommonUserException.class)
    protected ResponseEntity<CommonUserException> handleUserException(CommonUserException e) {
        log.debug(HANDLER_MESSAGE, e.getClass().getSimpleName());
        log.error("Unknown server error occured", e); // error, not warn!

        return new ResponseEntity<>(e, INTERNAL_SERVER_ERROR);
    }

    /**
     * Not found exception handling (404)
     *
     * @param e exception
     * @return response entity with CommonUserException
     */
    @ExceptionHandler(NotFoundUserException.class)
    protected ResponseEntity<CommonUserException> handleUserException(NotFoundUserException e) {
        log.debug(HANDLER_MESSAGE, e.getClass().getSimpleName());
        log.warn("Entity not found occured", e); // just warn (due user do wrong), not our error

        return new ResponseEntity<>(e, NOT_FOUND);
    }

    @ExceptionHandler(NoAccessUserException.class)
    protected ResponseEntity<CommonUserException> handleUserException(NoAccessUserException e) {
        log.debug(HANDLER_MESSAGE, e.getClass().getSimpleName());
        log.warn("No access occured", e); // just warn (due user do wrong), not our error

        return new ResponseEntity<>(e, FORBIDDEN);
    }

    /**
     * Multiple bad parameters exception handling (400)
     *
     * @param e exception
     * @return response entity with CommonUserException
     */
    @ExceptionHandler({ValidationUserException.class, IllegalOperationUserException.class})
    protected ResponseEntity<CommonUserException> handleValidationUserException(CommonUserException e) {
        log.warn("Validation error occured", e); // just warn (due user do wrong), not our error
        log.debug(HANDLER_MESSAGE, e.getClass().getSimpleName());
        return new ResponseEntity<>(e, BAD_REQUEST);
    }

    /**
     * Overriding spring default handling for @Valid controller argument
     */
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

        log.warn("Controller method argument is not valid", ex);
        ValidationUserException validationUserException = buildValidationUserException(ex.getBindingResult());
        return new ResponseEntity<>(validationUserException, BAD_REQUEST);
    }

    /**
     * Overriding spring default handling for @Valid when mapping GET params to POJO argument
     */
    @Override
    protected ResponseEntity<Object> handleBindException(BindException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        log.warn("Controller binding is not valid", ex); // just warn (due user do wrong), not our error
        ValidationUserException validationUserException = buildValidationUserException(ex.getBindingResult());
        return new ResponseEntity<>(validationUserException, BAD_REQUEST);
    }

    private ValidationUserException buildValidationUserException(BindingResult result) {

        //ex.getBindingResult().getAllErrors().get(1).getDefaultMessage()

        ValidationUserException finalResult = new ValidationUserException(
                VALIDATION_FAILED,
                "Form {} validation failed, see badParams section for per-field errors",
                arrayToMap("form", result.getObjectName())
        );


        List<CommonUserException> allErrors = result.getAllErrors().stream().map(
                err -> {
                    if (err instanceof FieldError) {
                        FieldError fieldError = (FieldError)err;
                        return new BadParamUserException.BadFormatBuilder(fieldError.getField(), err.getDefaultMessage(), fieldError.getRejectedValue()).build();
                    }
                    else
                        return new ValidationUserException(err.getDefaultMessage());
                }
        ).collect(Collectors.toList());

        finalResult.addConstraintViolations(allErrors);

        return finalResult;
    }



    /**
     * General server error (500)
     * <p>
     * All throwables that haven't been handle by Spring are being handled here
     * (note: only those which weren't intercepted by Spring).
     * For example: IllegalArgument exception or NullPointer exception.
     *
     * @param throwable exception
     * @return response entity with CommonUserException
     */
    @ExceptionHandler(Throwable.class)
    protected ResponseEntity<CommonUserException> handleThrowable(Throwable throwable) {
        log.error("Unknown exception occurred", throwable); // error cause some unknown throwable thrown
        CommonUserException clientError = new InternalErrorUserException("Unknown server error occurred");
        return new ResponseEntity<>(clientError, INTERNAL_SERVER_ERROR);
    }

    /**
     * Method is not supported (405)
     *
     * @param exception exception
     * @return response entity with CommonUserException
     */
    @Override
    protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(HttpRequestMethodNotSupportedException exception, HttpHeaders headers, HttpStatus status, WebRequest request) {
        String method = exception.getMethod();
        log.warn("Method {} is not supported", method);
        CommonUserException clientError = new InternalErrorUserException("Method {} is not supported", arrayToMap("method", method));
        return new ResponseEntity<>(clientError, METHOD_NOT_ALLOWED);
    }

    /**
     * When required parameter is absent
     *
     * @param exception missing param exception
     * @param headers   headers
     * @param status    status
     * @param request   request
     * @return response entity
     */
    @Override
    protected ResponseEntity<Object> handleMissingServletRequestParameter(
            MissingServletRequestParameterException exception,
            HttpHeaders headers, HttpStatus status, WebRequest request
    )
    {
        log.warn("Servlet request parameter is missing", exception);

        return new ResponseEntity<>(new BadParamUserException.BadFormatBuilder(
                exception.getParameterName(), "missing of type '" + exception.getParameterType() + "'", null
        ).build(), BAD_REQUEST);
    }

    /**
     * When param type mismatch
     *
     * @param e       type mismatch exception
     * @param headers headers
     * @param status  status
     * @param request request
     * @return response entity
     */
    @Override
    protected ResponseEntity<Object> handleTypeMismatch(TypeMismatchException e, HttpHeaders headers, HttpStatus status, WebRequest request)
    {
        log.warn("Parameter type mismatch", e);
        return new ResponseEntity<>(new BadParamUserException.BadFormatBuilder(
                e.getPropertyName(), e.getErrorCode(), e.getValue()
        ).build(), BAD_REQUEST);
    }

    /**
     * When path variable is absent
     *
     * @param e       missing path variable exception
     * @param headers headers
     * @param status  status
     * @param request request
     * @return response entity
     */
    @Override
    protected ResponseEntity<Object> handleMissingPathVariable(MissingPathVariableException e, HttpHeaders headers, HttpStatus status, WebRequest request)
    {
        log.warn("Request path variable is missing", e);
        return new ResponseEntity<>(new ValidationUserException(
                BAD_PARAMETER_MISSING,
                "Path variable {} ({}) is missing",
                arrayToMap("name", e.getVariableName(), "paramName", e.getParameter().getParameterName())
        ), BAD_REQUEST);
    }

    /**
     * When servlet binding fails
     *
     * @param e       failed binding exception
     * @param headers headers
     * @param status  status
     * @param request request
     * @return response entity
     */
    @Override
    protected ResponseEntity<Object> handleServletRequestBindingException(ServletRequestBindingException e, HttpHeaders headers, HttpStatus status, WebRequest request)
    {
        log.error("Binding is wrong", e);
        return new ResponseEntity<>(new ValidationUserException(
                BAD_PARAMETER_FORMAT,
                "Unable to read incoming parameters. General binding error"
        ), BAD_REQUEST);
    }

    /**
     * When HTTP message is not writable
     * <p>
     * Thrown by {@link HttpMessageConverter} implementations when the
     * {@link HttpMessageConverter#write} method fails.
     * <p>
     * Example: causes when entity can't be serialized to JSON (for some reason)
     *
     * @param e       HttpMessageNotWritableException
     * @param headers headers
     * @param status  status
     * @param request request
     * @return response entity
     */
    @Override
    protected ResponseEntity<Object> handleHttpMessageNotWritable(HttpMessageNotWritableException e, HttpHeaders headers, HttpStatus status, WebRequest request)
    {
        log.error("Http message is not writable", e);
        return new ResponseEntity<>(
                new InternalErrorUserException(UNKNOWN_SERVER_ERROR, "HTTP message is not writable"),
                INTERNAL_SERVER_ERROR
        );
    }

    /**
     * When HTTP message is not readable.
     * In practice it happens even if input JSON not corresponds to class you expected
     * (e.g. some extra fields and expected class not marked as field-ignorable (@JsonIgnoreProperties(ignoreUnknown = true)))
     * @param ex exception
     * @param headers headers
     * @param status status
     * @param request request
     * @return response entity
     */
    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request)
    {
        log.warn("Cannot read http message", ex);
        return new ResponseEntity<>(new ValidationUserException(
                BAD_PARAMETER_FORMAT,
                "Unable to parse incoming HTTP request or its body is missing"
        ), BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleMissingServletRequestPart(MissingServletRequestPartException ex, HttpHeaders headers, HttpStatus status, WebRequest request)
    {
        log.error("Missing part '{}' of multipart request", ex.getRequestPartName(), ex);
        return new ResponseEntity<>(new BadParamUserException.CommonBuilder(
                ex.getRequestPartName(),
                null
        ).build(), HttpStatus.BAD_REQUEST);
    }
}
