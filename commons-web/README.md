# COMMONS-WEB MODULE

Useful classes for common usage with web (spring-boot, spring-mvc).
It is *NOT the part of DigitalHarbour*.
This module might be separated and used for any java server-side project.

Only inner dependency is to [commons](../commons/README.md) module.
